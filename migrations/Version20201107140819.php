<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201107140819 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, message_id INT NOT NULL, text VARCHAR(140) NOT NULL, date DATE NOT NULL, INDEX IDX_9474526CF675F31B (author_id), INDEX IDX_9474526C537A1329 (message_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment_like (id INT AUTO_INCREMENT NOT NULL, comment_id INT NOT NULL, author_id INT NOT NULL, INDEX IDX_8A55E25FF8697D13 (comment_id), INDEX IDX_8A55E25FF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_moderator (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, message VARCHAR(255) NOT NULL, date DATETIME NOT NULL, INDEX IDX_4C19113F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE follow (id INT AUTO_INCREMENT NOT NULL, follow_target_id INT NOT NULL, follow_source_id INT NOT NULL, INDEX IDX_6834447094FB22ED (follow_target_id), INDEX IDX_68344470144935EA (follow_source_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hashtag (id INT AUTO_INCREMENT NOT NULL, text VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hashtag_message (hashtag_id INT NOT NULL, message_id INT NOT NULL, INDEX IDX_8DDD4259FB34EF56 (hashtag_id), INDEX IDX_8DDD4259537A1329 (message_id), PRIMARY KEY(hashtag_id, message_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, text VARCHAR(140) NOT NULL, date DATETIME NOT NULL, image_name VARCHAR(255) DEFAULT NULL, image_type VARCHAR(255) DEFAULT NULL, INDEX IDX_B6BD307FF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message_like (id INT AUTO_INCREMENT NOT NULL, message_id INT NOT NULL, author_id INT NOT NULL, INDEX IDX_5F6DB6A537A1329 (message_id), INDEX IDX_5F6DB6AF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE report (id INT AUTO_INCREMENT NOT NULL, messageid_id INT DEFAULT NULL, retweetid_id INT DEFAULT NULL, commentid_id INT DEFAULT NULL, userid_id INT DEFAULT NULL, message VARCHAR(255) NOT NULL, date DATETIME NOT NULL, INDEX IDX_C42F77848BF47595 (messageid_id), INDEX IDX_C42F778491178380 (retweetid_id), INDEX IDX_C42F77844574CA0 (commentid_id), INDEX IDX_C42F778458E0A285 (userid_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE retweet (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, message_id INT NOT NULL, text VARCHAR(140) DEFAULT NULL, date DATE NOT NULL, INDEX IDX_45E67DB3F675F31B (author_id), INDEX IDX_45E67DB3537A1329 (message_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, pseudo VARCHAR(45) NOT NULL, bio VARCHAR(255) DEFAULT NULL, created_at DATE NOT NULL, is_verified TINYINT(1) NOT NULL, avatar_name VARCHAR(255) DEFAULT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D64986CC499D (pseudo), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C537A1329 FOREIGN KEY (message_id) REFERENCES message (id)');
        $this->addSql('ALTER TABLE comment_like ADD CONSTRAINT FK_8A55E25FF8697D13 FOREIGN KEY (comment_id) REFERENCES comment (id)');
        $this->addSql('ALTER TABLE comment_like ADD CONSTRAINT FK_8A55E25FF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE contact_moderator ADD CONSTRAINT FK_4C19113F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE follow ADD CONSTRAINT FK_6834447094FB22ED FOREIGN KEY (follow_target_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE follow ADD CONSTRAINT FK_68344470144935EA FOREIGN KEY (follow_source_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE hashtag_message ADD CONSTRAINT FK_8DDD4259FB34EF56 FOREIGN KEY (hashtag_id) REFERENCES hashtag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hashtag_message ADD CONSTRAINT FK_8DDD4259537A1329 FOREIGN KEY (message_id) REFERENCES message (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE message_like ADD CONSTRAINT FK_5F6DB6A537A1329 FOREIGN KEY (message_id) REFERENCES message (id)');
        $this->addSql('ALTER TABLE message_like ADD CONSTRAINT FK_5F6DB6AF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F77848BF47595 FOREIGN KEY (messageid_id) REFERENCES message (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F778491178380 FOREIGN KEY (retweetid_id) REFERENCES retweet (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F77844574CA0 FOREIGN KEY (commentid_id) REFERENCES comment (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F778458E0A285 FOREIGN KEY (userid_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE retweet ADD CONSTRAINT FK_45E67DB3F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE retweet ADD CONSTRAINT FK_45E67DB3537A1329 FOREIGN KEY (message_id) REFERENCES message (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment_like DROP FOREIGN KEY FK_8A55E25FF8697D13');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F77844574CA0');
        $this->addSql('ALTER TABLE hashtag_message DROP FOREIGN KEY FK_8DDD4259FB34EF56');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C537A1329');
        $this->addSql('ALTER TABLE hashtag_message DROP FOREIGN KEY FK_8DDD4259537A1329');
        $this->addSql('ALTER TABLE message_like DROP FOREIGN KEY FK_5F6DB6A537A1329');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F77848BF47595');
        $this->addSql('ALTER TABLE retweet DROP FOREIGN KEY FK_45E67DB3537A1329');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F778491178380');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CF675F31B');
        $this->addSql('ALTER TABLE comment_like DROP FOREIGN KEY FK_8A55E25FF675F31B');
        $this->addSql('ALTER TABLE contact_moderator DROP FOREIGN KEY FK_4C19113F675F31B');
        $this->addSql('ALTER TABLE follow DROP FOREIGN KEY FK_6834447094FB22ED');
        $this->addSql('ALTER TABLE follow DROP FOREIGN KEY FK_68344470144935EA');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FF675F31B');
        $this->addSql('ALTER TABLE message_like DROP FOREIGN KEY FK_5F6DB6AF675F31B');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F778458E0A285');
        $this->addSql('ALTER TABLE retweet DROP FOREIGN KEY FK_45E67DB3F675F31B');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE comment_like');
        $this->addSql('DROP TABLE contact_moderator');
        $this->addSql('DROP TABLE follow');
        $this->addSql('DROP TABLE hashtag');
        $this->addSql('DROP TABLE hashtag_message');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE message_like');
        $this->addSql('DROP TABLE report');
        $this->addSql('DROP TABLE retweet');
        $this->addSql('DROP TABLE user');
    }
}
