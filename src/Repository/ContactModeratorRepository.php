<?php

namespace App\Repository;

use App\Entity\ContactModerator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContactModerator|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactModerator|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactModerator[]    findAll()
 * @method ContactModerator[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactModeratorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactModerator::class);
    }

    // /**
    //  * @return ContactModerator[] Returns an array of ContactModerator objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContactModerator
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
