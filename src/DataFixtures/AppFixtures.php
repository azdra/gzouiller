<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\CommentLike;
use App\Entity\Follow;
use App\Entity\Hashtag;
use App\Entity\Message;
use App\Entity\MessageLike;
use App\Entity\Retweet;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $faker_users = array();
        $userRepository = $manager->getRepository(User::class);
        $hashtagRepository = $manager->getRepository(Hashtag::class);
        $messageRepository = $manager->getRepository(Message::class);
        $messageCount = 0;
        $userCount = 0;

        // Default
        if ($userRepository->findOneBy(['email'=>"yellow@mail.com"]) === null) {
            $user = new User();
            $user->setEmail("yellow@mail.com");
            $user->setBio("Ce qui est mystique ce n’est pas comment est le monde, mais le fait qu’il est.");
            $pass = password_hash("password", PASSWORD_ARGON2I);
            $user->setPassword($pass);
            $user->setRoles(["ROLE_ADMIN", "ROLE_ALLOWED_TO_SWITCH"]);
            $user->setPseudo("Yellow");
            $user->setCreatedAt(new \DateTime());
            $user->setUpdatedAt(new \DateTime());
            $manager->persist($user);
            $manager->flush();
        }

        for ($i = 0; $i < 10; $i++) {

            // Creation d'un nouveau user
            $userCount++;
            $user = new User();
            $email = $faker->email;
            while($userRepository->findOneBy(['email'=>$email]) !== null) {
                $email = $faker->email;
            }
            $user->setEmail($email);
            $fakerPass = password_hash($faker->password, PASSWORD_ARGON2I);
            $user->setPassword($fakerPass);
            $pseudo = str_replace(" ", "", $faker->unique()->lastName);
            while($userRepository->findOneBy(['pseudo'=>$pseudo]) !== null) {
                $pseudo = str_replace(" ", "", $faker->unique()->lastName);
            }
            $faker_users[] = $pseudo;
            $user->setPseudo($faker_users[$i]);
            $user->setBio($faker->text($maxNbChars = 140));
            $date = $faker->dateTimeInInterval($startDate = '-7 days', $interval = '+ 5 days', $timezone = null);
            $user->setCreatedAt($date);
            $user->setUpdatedAt($date);
            $manager->persist($user);

            // Creation d'un nouveau message
            $messageNb = random_int(1, 3);
            for ($m = 0; $m < $messageNb; $m++) {
                $messageCount++;
                $message = new Message();
                $text = $faker->text($maxNbChars = 140);
                $message->setText($text);
                $message->setAuthor($user);
                $message->setDate($date);
                $manager->persist($message);

                // Création de mention
                if (random_int(0, 1)) {
                    $text = "@".$userRepository->find(random_int(1, $userCount))->getPseudo()." ".$text;
                }
                // Création de hashtag
                $words = str_word_count($text, 1);
                $wordNb = count($words);
                foreach ($words as $index => $tag) {
                    if (substr($tag, 0, 1) !== "@" && random_int(0, $wordNb) === 0) {
                        $words[$index] = "#" . $tag;
                        $hashtag = $hashtagRepository->findOneBy(["text" => $tag]) ?? new Hashtag();
                        $hashtag->setText($tag);
                        $hashtag->addMessage($message);
                        $manager->persist($hashtag);
                        $message->addHashtag($hashtag);
                    }
                }

                // Raccourcir le texte pour respecter la limitation de taille
                if (strlen($text) > 140) {
                    $text = substr($text, 0,140);
                }
                $message->setText($text);
                $manager->persist($message);

                // Creation d'un nouveau commentaire
                $commentNb = random_int(1, 3);
                for ($c = 0; $c < $commentNb; $c++) {
                    $comment = new Comment();
                    $comment->setText($faker->text($maxNbChars = 140));
                    $comment->setMessage($message);
                    $comment->setAuthor($user);
                    $commentDate = $c+1;
                    $comment->setDate($date->modify("+$commentDate day"));
                    $manager->persist($comment);
                    $message->addComment($comment);
                    $manager->persist($message);

                    // Creation d'un like sur comment
                    if (random_int(0, 1)) {
                        $comment_like = new CommentLike();
                        $comment_like->setAuthor($user);
                        $comment_like->setComment($comment);
                        $manager->persist($comment_like);
                    }
                }
                $manager->flush();
            }

            for ($l = 1; $l <= $messageCount; $l++) {
                $message = $messageRepository->find($l);
                if (is_null($message)) {
                    continue;
                }
                // Creation d'un like sur message
                if (random_int(0, 1)) {
                    $message_like = new MessageLike();
                    $message_like->setAuthor($user);
                    $message_like->setMessage($message);
                    $manager->persist($message_like);
                    $message->addLike($message_like);
                    $manager->persist($message);
                }

                // Creation d'un retweet
                if (random_int(0, 1)) {
                    $retweet = new Retweet();
                    $retweet->setMessage($message);
                    if ($i % 2 === 0) {
                        $retweet->setText($faker->text($maxNbChars = 140));
                    }
                    $retweet->setAuthor($user);
                    $retweetDate = $l+1;
                    $retweet->setDate($date->modify("+$retweetDate day"));
                    $manager->persist($retweet);
                    $message->addRetweet($retweet);
                    $manager->persist($message);
                }
            }

            $manager->flush();
        }

        // Création de follows
        for ($i = 1; $i <= 11; $i++) {
            $user = $userRepository->find($i);
            if (is_null($user)) {
                continue;
            }
            for ($j = 1; $j <= 11; $j++) {
                if ($i !== $j && random_int(0,1)) {
                    $followTarget = $userRepository->find($j);
                    $follow = new Follow();
                    $follow->setFollowTarget($followTarget);
                    $followTarget->addFollower($follow);
                    $follow->setFollowSource($user);
                    $user->addFollowing($follow);
                }
            }
        }
        $manager->flush();
    }
}
