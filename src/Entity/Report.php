<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ReportRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 * @ApiResource
 */
class Report
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity=Message::class, inversedBy="reports")
     */
    private $messageid;

    /**
     * @ORM\ManyToOne(targetEntity=Retweet::class, inversedBy="reports")
     */
    private $retweetid;

    /**
     * @ORM\ManyToOne(targetEntity=Comment::class, inversedBy="reports")
     */
    private $commentid;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reports")
     */
    private $userid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getMessageid(): ?Message
    {
        return $this->messageid;
    }

    public function setMessageid(?Message $messageid): self
    {
        $this->messageid = $messageid;

        return $this;
    }

    public function getRetweetid(): ?Retweet
    {
        return $this->retweetid;
    }

    public function setRetweetid(?Retweet $retweetid): self
    {
        $this->retweetid = $retweetid;

        return $this;
    }

    public function getCommentid(): ?Comment
    {
        return $this->commentid;
    }

    public function setCommentid(?Comment $commentid): self
    {
        $this->commentid = $commentid;

        return $this;
    }

    public function getUserid(): ?User
    {
        return $this->userid;
    }

    public function setUserid(?User $userid): self
    {
        $this->userid = $userid;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
