<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FollowRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=FollowRepository::class)
 * @UniqueEntity(fields={"follow_target", "follow_source"})
 */
class Follow
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="followers")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotIdenticalTo(propertyPath="follow_source")
     */
    private $follow_target;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="followings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $follow_source;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFollowTarget(): ?User
    {
        return $this->follow_target;
    }

    public function setFollowTarget(?User $follow_target): self
    {
        $this->follow_target = $follow_target;

        return $this;
    }

    public function getFollowSource(): ?User
    {
        return $this->follow_source;
    }

    public function setFollowSource(?User $follow_source): self
    {
        $this->follow_source = $follow_source;

        return $this;
    }
}
