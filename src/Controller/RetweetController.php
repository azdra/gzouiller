<?php

namespace App\Controller;

use App\Form\NewRetweetType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RetweetController extends AbstractController
{
    /**
     * @Route("/retweet", name="new_retweet", methods={"GET"})
     * @param Request $request
     * @return Response
     * @IsGranted("ROLE_USER")
     */
    public function new(Request $request): Response
    {
        $id = $request->query->get("message");
        $form = $this->createForm(NewRetweetType::class);
        return $this->render('retweet/new.html.twig', [
            'messageId' => $id,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/retweet/{id}", name="show_retweet", methods={"GET"}, requirements={"id"="\d+"})
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        return $this->render('retweet/show.html.twig', ['id'=>$id]);
    }
}
