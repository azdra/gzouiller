<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditProfilType;
use App\Form\NewMessageType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/profil", name="name_")
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/", name="redirectMyProfile")
     * @return RedirectResponse
     */
    public function redirectMyProfile()
    {
        return $this->redirectToRoute('name_myProfile');
    }

    /**
     * @Route("/@me", name="myProfile", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function myProfile(UserRepository $userRepository): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $form = $this->createForm(NewMessageType::class);
        return $this->render('profil/index.html.twig', [
            'form' => $form->createView(),
            'user' => $user
        ]);
    }

    /**
     * @Route("/@{pseudo}", name="user_profile", methods={"GET"}, requirements={"pseudo"="\w+"})
     * @param string $pseudo
     * @param UserRepository $userRepository
     * @return Response
     */
    public function userProfile($pseudo, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(['pseudo'=>$pseudo]);
        $form = $this->createForm(NewMessageType::class);
        return $this->render('profil/index.html.twig', [
            'form' => $form->createView(),
            'user' => $user
        ]);
    }

    /**
     * @Route("/@me/edit", name="myProfileUpdate", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function myProfileUpdate(): Response
    {

        $form = $this->createForm(EditProfilType::class, $this->getUser());
        return $this->render('profil/_edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

}
