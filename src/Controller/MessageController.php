<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\NewMessageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageController extends AbstractController
{
    /**
     * @Route("/message", name="message_new")
     * @IsGranted("ROLE_USER")
     */
    public function new(): Response
    {
        $MAX_SIZE = 1000000;
        $form = $this->createForm(NewMessageType::class, null, ['max_size' => $MAX_SIZE]);
        return $this->render('message/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/message/{id}", name="message_show", requirements={"id"="\d+"})
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        return $this->render('message/show.html.twig', [
            'id' => $id
        ]);
    }
}
