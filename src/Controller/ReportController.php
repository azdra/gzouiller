<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReportController extends AbstractController
{
    /**
     * @Route("/report/{type}/{id}", name="report", requirements={"id"="\d+", "type"="message|comment|user"})
     * @param string $type Type du report {message|comment|user}
     * @param int $id
     * @return Response
     */
    public function index(string $type, int $id): Response
    {
        return $this->render('report/index.html.twig', [
            'type' => $type,
            'id' => $id
        ]);
    }
}
