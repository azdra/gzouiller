<?php

namespace App\Controller;

use App\Entity\ContactModerator;
use App\Entity\Hashtag;
use App\Entity\Message;
use App\Entity\User;
use App\Repository\HashtagRepository;
use App\Repository\ReportRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/user/{id}", name="api_user", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function myProfile(EntityManagerInterface $entityManager): Response
    {
        $repo = $entityManager->getRepository(User::class);
        $userProperties = $repo->find($this->getUser()->getId());

        $user['pseudo'] = $userProperties->getPseudo();
        $user['bio'] = $userProperties->getBio();
        $user['avatar'] = $userProperties->getAvatarFile();
        $user['created_at'] = $userProperties->getCreatedAt();

        return $this->json($user, 200);
    }

    /**
     * @Route("/api/messages", name="message_save", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws \Exception
     */
    public function messageSave(Request $request, EntityManagerInterface $entityManager) : Response
    {
        $data = json_decode($request->getContent(), true);
        $author = $entityManager->getRepository(User::class)->find(["id"=>$data["author"]]);
        $date = new \DateTime($data["date"]);
        $text = $data["text"];

        // invalid request
        if (is_null($author) || is_null($date) || is_null($text) || strlen($text) > 140 || strlen($text) === 0) {
            return $this->json(["error"=>"Invalid input"], 400);
        }

        $message = new Message();
        $message->setDate($date);
        $message->setText($text);
        $message->setAuthor($author);

        $entityManager->persist($message);

        // attached file
        if (null !== $request->files->get("new_message") && array_key_exists("file", $request->files->get("new_message")["imageFile"]) && !is_null($request->files->get("new_message")["imageFile"]["file"])) {
            $file = $request->files->get("new_message")["imageFile"]["file"];
            $message->setImageFile($file);
            $message->setImageType($file->getMimeType());
            $message->setImageName($file->getClientOriginalName());
        }

        /*
         * ## System d'Hashtag MESSAGE ## *
            Récupère tous les mots commençant par #
            #Confinement ce soir #PasDidéeDexemple
            return [["#Confinement","#PasDidéeDexemple"],["Confinement","PasDidéeDexemple"]]
        */
        preg_match_all('/#(\w+)/',$text,$matches);
        $hashtagRepository = $entityManager->getRepository(Hashtag::class);
        foreach ($matches[1] as $match) {
            // Créer un nouveau Hashtag en ajoutant le # + l'id du message
            $hashtag = $hashtagRepository->findOneBy(["text"=>$match]) ?? new Hashtag();
            $hashtag->addMessage($message);
            $message->addHashtag($hashtag);
            $entityManager->persist($hashtag);
        }

        $entityManager->flush();

        return $this->json([
            "id" => $message->getId(),
            "status" => 201,
            "text" => $text,
            "hashtag" => $matches[1] ? $matches[1] : null
        ], 201);
    }

    /**
     * @Route("/api/contact_moderators", name="apicontactModerator", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws \Exception
     */
    public function apicontactModerator(Request $request, EntityManagerInterface $entityManager) : Response
    {
        $data = json_decode($request->getContent(), true);
        $author = $entityManager->getRepository(User::class)->find(['id'=>$data['author']]);
        $date = new \DateTime();
        $text = $data['message'];

        $contact = new ContactModerator();
        $contact->setDate($date);
        $contact->setAuthor($author);
        $contact->setMessage($text);

        $entityManager->persist($contact);
        $entityManager->flush();

        return $this->json([
            'id' => $contact->getId(),
            'author' => $contact->getAuthor(),
            'message' => $contact->getMessage(),
            'status' => 201,
            'statusText' => 'OK'
        ], 201);
    }

    /**
     * @Route("/api/avatar/{id}", name="save_avatar", requirements={"id"="\d+"}, methods={"POST"})
     * @param User $user
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function saveAvatar(User $user, Request $request, EntityManagerInterface $entityManager)
    {
        if (!is_null($request->files->get("file"))) {
            $file = $request->files->get("file");
            $user->setAvatarFile($file);
            $user->setAvatarName($file->getClientOriginalName());
            $user->setUpdatedAt(new \DateTime());
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->json(["result" => "Success"]);
        } else {
            return $this->json(["result" => "Bad input"], 400);
        }
    }

    /**
     * @Route("/api/attached/{id}", name="save_attached", requirements={"id"="\d+"}, methods={"POST"})
     * @param Message $message
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function saveAttached(Message $message, Request $request, EntityManagerInterface $entityManager)
    {
        if (!is_null($request->files->get("file"))) {
            $file = $request->files->get("file");
            $message->setImageFile($file);
            $message->setImageName($file->getClientOriginalName());
            $message->setImageType($file->getMimeType());
            $entityManager->persist($message);
            $entityManager->flush();
            return $this->json(["result" => "Success"]);
        } else {
            return $this->json(["result" => "Bad input"], 400);
        }
    }

    /**
     * @Route("/api/attached/{id}", name="get_attached", requirements={"id"="\d+"}, methods={"GET"})
     * @param Message $message
     * @param UploaderHelper $helper
     * @return BinaryFileResponse
     */
    public function getAttached(Message $message, UploaderHelper $helper)
    {
        $path = $helper->asset($message);
        return new BinaryFileResponse('..'.$path);
    }

    /**
     * @Route("/api/hashtags/like/{text}", name="api_hashtags_like", requirements={"text"="\w+"}, methods={"GET"})
     * @param $text
     * @param HashtagRepository $hashtagRepository
     * @return JsonResponse
     */
    public function hashNameLikeGet($text, HashtagRepository $hashtagRepository)
    {
        $hashtags = $hashtagRepository->findTextLike($text);
        // building the response
        $json = [];
        foreach ($hashtags as $hashtag) {
            $json[] = ['text' => $hashtag->getText()];
        }
        return $this->json($json);
    }

    /**
     * @Route("/api/users/like/{pseudo}", name="api_users_like", requirements={"pseudo"="\w+"}, methods={"GET"})
     * @param $pseudo
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function userPseudoLikeGet($pseudo, UserRepository $userRepository)
    {
        $users = $userRepository->findPseudoLike($pseudo);

        // building the response
        $json = [];
        foreach ($users as $user) {
            $followersSerial = [];
            $followers = $user->getFollowers();
            foreach ($followers as $follower) {
                $followersSerial[] = ['id' => $follower->getId()];
            }
            $json[] = [
                'id' => $user->getId(),
                'pseudo' => $user->getPseudo(),
                'followers' => $followersSerial
            ];
        }

        return $this->json($json);
    }

    /**
     * @Route("/api/hashtags/{name}", name="api_hashtags_get_item_name", requirements={"name"="\D+"}, methods={"GET"})
     * @param $name
     * @param HashtagRepository $hashtagRepository
     * @return RedirectResponse
     */
    public function hashNameGet($name, HashtagRepository $hashtagRepository)
    {
        $id = $hashtagRepository->findOneBy(["text"=>$name])->getId();
        return $this->redirect("/api/hashtags/$id");
    }

    /**
     * @Route("/api/users/messages/{id}", name="api_get_user_recent_messages", requirements={"id"="\d+"}, methods={"GET"})
     * @param User $user
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function userRecentMessages(User $user, UserRepository $userRepository)
    {
        $messages = $userRepository->findRecentMessages($user->getId());
        return $this->json($messages);
    }

    /**
     * @Route("/api/reports/ordered", name="get_ordered_reports", methods={"GET"})
     * @param ReportRepository $repository
     * @return JsonResponse
     */
    public function orderedReports(ReportRepository $repository)
    {
        $reports = $repository->orderedByTarget();
        return $this->json($reports);
    }
}
