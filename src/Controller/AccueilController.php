<?php

namespace App\Controller;

use App\Form\NewMessageType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index(): Response
    {
        $form = $this->createForm(NewMessageType::class);
        return $this->render('accueil/index.html.twig', [
            'form' => $form->createView()

        ]);
    }

}
