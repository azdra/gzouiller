<?php

namespace App\Controller;

use App\Entity\ContactModerator;
use App\Form\ContactModeratorType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactModeratorController extends AbstractController
{
    /**
     * @Route("/contact/", name="contact_moderator")
     * @IsGranted("ROLE_USER")
     */
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        return $this->render('contact_moderator/index.html.twig', [
            'user' => $this->getUser(),
            'controller_name' => 'ContactModeratorController',
        ]);
    }
}
