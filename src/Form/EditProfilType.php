<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Vich\UploaderBundle\Form\Type\VichImageType;

class EditProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pseudo', TextType::class, [
                'required' => true
            ])
            ->add('bio', TextType::class, [
                'required' => false
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'mapped' => false,
                'required' => false,
                'invalid_message' => "Mot de passe.noduplicate",
                'first_options' => [
                    'label' => 'Mot de passe',
                    'attr' => [
                        'value' => '',
                        'placeholder' => 'Mot de passe.placeholder'
                    ]
                ],
                'second_options' => [
                    'label' => 'Répétez le mot de passe',
                    'attr' => [
                        'value' => '',
                        'placeholder' => 'Mot de passe.placeholder'
                    ]
                ],
                'constraints' => [
                    new Length([
                        'min' => 6,
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('avatarFile', VichImageType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'attached.placeholder',
                    'accept' => 'audio/*, video/*, image/*'
                ]
            ]);


//            ->add('email')
//            ->add('roles')
//            ->add('password')
//            ->add('pseudo')
//            ->add('bio')
//            ->add('created_at')
//            ->add('isVerified')
//            ->add('avatarName')
//            ->add('updatedAt')
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

