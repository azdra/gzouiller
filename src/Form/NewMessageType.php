<?php

namespace App\Form;

use App\Entity\Message;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Vich\UploaderBundle\Form\Type\VichFileType;

class NewMessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', TextareaType::class, [
                'required' => true,
                'label' => false,
                'attr' => [
                    'autofocus' => true,
                    'maxlength' => 140,
                    'placeholder' => 'Do like Trump, Share your actual mood !',
                    'id' => 'idNewMessage'
                ]
            ])
            ->add('imageFile', VichFileType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'lang' => 'fr',
                    'placeholder' => 'Add a file (Audio, Video, Image)',
                    'accept' => 'audio/*, video/*, image/*'
                ],
                'row_attr' => ['id' => 'new-message-file'],
                'constraints' => [
                    new File([
                        'maxSize' => $options['max_size'],
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                            'image/bmp',
                            'image/svg+xml',
                            'image/gif',
                            'image/webp',
                            'video/x-msvideo',
                            'video/mpeg',
                            'video/ogg',
                            'video/webm',
                            'video/3gpp',
                            'video/3gpp2',
                            'audio/aac',
                            'audio/midi',
                            'audio/ogg',
                            'audio/x-wav',
                            'audio/webm',
                            'audio/3gpp',
                            'audio/3gpp2',
                        ]
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Message::class,
            'max_size' => 2000000
        ]);
    }
}
