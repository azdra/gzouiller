const Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment
// if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
  Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    // .setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('app', './assets/js/app.js')
    .addEntry('show-message', './assets/js/pages/show-message.js')
    .addEntry('edit-profile', './assets/js/pages/edit-profile.js')
    .addEntry('accueil', './assets/js/pages/accueil.js')
    .addEntry('search', './assets/js/pages/search.js')
    .addEntry('search-hashtag', './assets/js/pages/search-hashtag.js')
    .addEntry('chart', './assets/js/pages/chart.js')
    .addEntry('profil-user', './assets/js/pages/profil-user.js')
    .addEntry('retweet', './assets/js/pages/retweet.js')
    .addEntry('show-retweet', './assets/js/pages/show-retweet.js')
    .addEntry('delete-user', './assets/js/forms/delete-user.js')
    .addEntry('new-message', './assets/js/forms/new-message.js')
    .addEntry('report', './assets/js/pages/report.js')
    .addEntry('preview_img', './assets/js/preview_img.js')
    .addEntry('contact_moderator', './assets/js/forms/contact_moderator.js')
    .addEntry('moderation', './assets/js/pages/moderation.js')
    .addEntry('preview_avatar', './assets/js/preview_avatar.js')
    .addEntry('admin_contact', './assets/js/pages/admin_contact.js')

    // When enabled, Webpack "splits" your files into smaller pieces
    // for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
      config.useBuiltIns = 'usage';
      config.corejs = 3;
    })

    // enables Sass/SCSS support
    .enableSassLoader()

// uncomment if you use TypeScript
// .enableTypeScriptLoader()

// uncomment to get integrity="..." attributes on your script & link tags
// requires WebpackEncoreBundle 1.4 or higher
// .enableIntegrityHashes(Encore.isProduction())

// uncomment if you're having problems with a jQuery plugin
// .autoProvidejQuery()

// uncomment if you use API Platform Admin (composer req api-admin)
// .enableReactPreset()
// .addEntry('admin', './assets/admin.js')

    .copyFiles({
      from: './assets/images',

      // optional target path, relative to the output dir
      to: 'images/[path][name].[ext]',

      // if versioning is enabled, add the file hash too
      // to: 'images/[path][name].[hash:8].[ext]',

      // only copy files matching this pattern
      pattern: /\.(png|jpg|jpeg|svg)$/,
    })
;

module.exports = Encore.getWebpackConfig();
