# Journal

## Jour 1 : 26/10

Début du Sprint 1 :

- Élaboration des users story (remplissage du product backlog)

- Nathalie : charte graphique

- Jean : modèle de la BDD

- Baptiste : dépôt Git et initialisation du projet Symfony

- Émilie & Julie : maquette

![trello le soir du 26/10](./trello-26-10.jpg)

## Jour 2 : 27/10

Continuation du Sprint 1 :

- Julie : prototypage, installation API Platform, formulaire de création de message

- Émilie : template page d'accueil, template navbar

- Jean : inscription, authentification

- Baptiste : style, templates messages

![trello 27/10](./trello-27-10.jpg)

## Jour 3 : 28/10

Fin Sprint 1 :

- Bonne communication dans l'équipe

- Product backlog et User Story bien mis en place

- Penser à mettre à jour le Trello pour le "En cours"

- Définition des "finis" manquante

Début Sprint 2 :

- Définition des "finis" effectuée

- Sélection des User Story à implémenter pendant ce sprint

- Julie : formulaire de création de message fonctionnel, champ avatar lors de l'inscription

- Jean : peaufiné l'UX des formulaires (placeholder, vérification de mot de passe, stylisation...), ajouté des Data Fixtures

- Baptiste : page paramètres de profil, style thème dark "mes abonnés" & "nouveau message"

- Nathalie : page de profil

- Émilie : 

![trello 28/10](./trello-28-10.jpg)

## Jour 4 : 29/10

Continuation du Sprint 2 :

- Jean : api pour le profil de l'utilisateur, ajout champ date de création, amélioré UX formulaires, débuggué les DataFixtures

- Émilie : publication de nouveaux messages depuis l'accueil et le profil, amélioration de la navbar

- Baptiste : amélioration du template de profil, récupération des messages de l'utilisateur (back)

- Julie : tests du formulaire de création de message

![trello 29/10](./trello-29-10.jpg)

## Jour 5 : 30/10

Fin du Sprint 2 :

- Jean : amélioration des styles (barre de recherche, navbar, couleurs, formulaires...), animation du logo

- Baptiste : les créations de #hashtags

- Émilie : blocage des formulaires quand non connecté

- Julie : visualisation d'un message + tests, page d'édition du profile (non fini)

![trello 30/10](./trello%2030-10.jpg)

## Jour 6 : 02/11

Début Sprint 3 :

- Émilie : requêtes AJAX pour création de message

- Jean : vue des abonnements et abonnés sur le profil, début des stats en back-office, copie facile de l'URL d'un message

- Baptiste : notifications avec SweetAlert, bouton "effacer un user" pour admin

- Julie : hashtags, fonction de recherche
  
  ![trello 02/11](./trello-02-11.jpg)
  
  ## Jour 7 : 03/11
  
  Continuation Sprint 3 :

- Julie : tests, script pour poster les messages en AJAX, récupération des messages récents sur les profils, retweets, peaufiné les data fixtures

- Émilie : script pour poster les messages en AJAX

- Baptiste : poster & afficher les commentaires

- Jean : continuation du back-office

## Jour 8 : 04/11

Fin Sprint 3 & Début du Sprint 4 :

- Jean : continuation des statistiques du back-office, peaufinage du style

- Baptiste : Like & Unlike de commentaires, amélioration de la création de commentaire

- Émilie : page CGU, stylisation des alertes

- Julie : rajout d'alertes pour redirection lors de création de messages & commentaires, ajout des pièces jointes aux messages en AJAX, validation JS du formulaire nouveau message
  
  ![trello 05/11](./trello-04-11.jpg)
  
  ## Jour 9 : 05/11
  
  Fin du sprint 4 :

- Baptiste : correction des likes des messages, ajout de l'option "signaler", page pour effectuer un signalement

- Julie : tests unitaires, ajout des mentions dans les messages, hashtags stockés à part, option "répondre" à un commentaire, peaufinage de l'UX des liens de messages

- Jean : terminé statistiques back-office, corrigé les fetch d'abonnés & abonnements sur profil, peaufiné menu optionsdes cartes

- Émilie : recherche & tutos

![trello 05/11](./trello-05-11.jpg)
