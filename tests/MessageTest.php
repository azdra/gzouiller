<?php

namespace App\Tests;

use App\Entity\Comment;
use App\Entity\Hashtag;
use App\Entity\Message;
use App\Entity\MessageLike;
use App\Entity\Report;
use App\Entity\Retweet;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MessageTest extends WebTestCase
{
    /**
     * @param $id
     * @dataProvider provideId
     */
    public function testRenduNonConnecteMessage($id)
    {
        $client = static::createClient();
        $crawler = $client->request('GET', "/message/$id");

        $this->assertResponseIsSuccessful();
        /* À TESTER COTÉ JS
        $this->assertEquals(1, $crawler->filter('[data-connected="false"]')->count());
        $this->assertEquals(0, $crawler->filter('[data-connected="true"]')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-author')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-abonnes')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-see-profile')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-follow')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-signal')->count());
        $this->assertEquals(0, $crawler->filter('#message-'.$id.'-delete')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-likes')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-likes-count')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-retweets')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-retweets-count')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-comments')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-comments-count')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-share')->count());*/
    }

    /**
     * @param $id
     * @dataProvider provideId
     */
    public function testRenduConnecteMessage($id)
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->find(2); // normal user
        $client->loginUser($user);

        $crawler = $client->request('GET', "/message/$id");

        $this->assertResponseIsSuccessful();
        /*$this->assertEquals(0, $crawler->filter('[data-connected="false"]')->count());
        $this->assertEquals(1, $crawler->filter('[data-connected="true"]')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-author')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-abonnes')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-see-profile')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-follow')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-signal')->count());
        $this->assertEquals(0, $crawler->filter('#message-'.$id.'-delete')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-likes')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-likes-count')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-retweets')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-retweets-count')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-comments')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-comments-count')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-share')->count());*/
    }

    /**
     * @param $id
     * @dataProvider provideId
     */
    public function testRenduConnecteAdminMessage($id)
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email'=>'yellow@mail.com']); // admin
        $client->loginUser($user);

        $crawler = $client->request('GET', "/message/$id");

        $this->assertResponseIsSuccessful();
        /*$this->assertEquals(0, $crawler->filter('[data-connected="false"]')->count());
        $this->assertEquals(1, $crawler->filter('[data-connected="true"]')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-author')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-abonnes')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-see-profile')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-follow')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-signal')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-delete')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-likes')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-likes-count')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-retweets')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-retweets-count')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-comments')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-comments-count')->count());
        $this->assertEquals(1, $crawler->filter('#message-'.$id.'-share')->count());*/
    }

    public function provideId()
    {
        return [
          ["1"],
          ["2"],
          ["3"],
          ["4"],
          ["5"],
        ];
    }

    // TESTS UNITAIRES
    public function testRetweetsMessage()
    {
        $message = new Message();
        $preCount = count($message->getRetweets());
        $entity = new Retweet();
        $message->addRetweet($entity);
        $this->assertCount($preCount+1, $message->getRetweets());
        $message->removeRetweet($entity);
        $this->assertCount($preCount, $message->getRetweets());
    }

    public function testLikesMessage()
    {
        $message = new Message();
        $preCount = count($message->getLikes());
        $entity = new MessageLike();
        $message->addLike($entity);
        $this->assertCount($preCount+1, $message->getLikes());
        $message->removeLike($entity);
        $this->assertCount($preCount, $message->getLikes());
    }

    public function testCommentsMessage()
    {
        $message = new Message();
        $preCount = count($message->getComments());
        $entity = new Comment();
        $message->addComment($entity);
        $this->assertCount($preCount+1, $message->getComments());
        $message->removeComment($entity);
        $this->assertCount($preCount, $message->getComments());
    }

    public function testHashtagsMessage()
    {
        $message = new Message();
        $preCount = count($message->getHashtags());
        $entity = new Hashtag();
        $message->addHashtag($entity);
        $this->assertCount($preCount+1, $message->getHashtags());
        $message->removeHashtag($entity);
        $this->assertCount($preCount, $message->getHashtags());
    }

    public function testReportsMessage()
    {
        $message = new Message();
        $preCount = count($message->getReports());
        $entity = new Report();
        $message->addReport($entity);
        $this->assertCount($preCount+1, $message->getReports());
        $message->removeReport($entity);
        $this->assertCount($preCount, $message->getReports());
    }
}
