<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InscriptionTest extends WebTestCase
{
    public function testInscriptionNouvelUtilisateur()
    {
        $client = static::createClient();
        $client->request('GET', '/register');

        $this->assertResponseIsSuccessful();
        $client->submitForm('submit', [
            'registration_form[email]' => 'ju@lie.sou',
            'registration_form[pseudo]' => 'julie',
            'registration_form[plainPassword][first]' => 'azerty',
            'registration_form[plainPassword][second]' => 'azerty',
            'registration_form[agreeTerms]' => '1',
        ]);
        $this->assertResponseRedirects('/', 302,'doit rediriger vers la page d\'accueil');

        $userRepository = static::$container->get(UserRepository::class);
        $count = count($userRepository->findBy(['email'=>'ju@lie.sou']));
        $this->assertEquals(1, $count);
    }

    public function testInscriptionDupliqueUtilisateur()
    {
        $client = static::createClient();
        $client->request('GET', '/register');

        $this->assertResponseIsSuccessful();
        $client->submitForm('submit', [
            'registration_form[email]' => 'yellow@mail.com',
            'registration_form[pseudo]' => 'julie',
            'registration_form[plainPassword][first]' => 'azerty',
            'registration_form[plainPassword][second]' => 'azerty',
            'registration_form[agreeTerms]' => '1',
        ]);
        $this->assertResponseIsSuccessful('reste sur la page d\'inscription, avec un message d\'erreur');
    }
}
