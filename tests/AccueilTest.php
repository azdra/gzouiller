<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AccueilTest extends WebTestCase
{
    public function testAccueil()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertEquals(1, $crawler->filter('.navbar')->count());
        $this->assertEquals(1, $crawler->filter('footer')->count());
    }

    public function testBarreDeRecherche()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertEquals(1, $crawler->filter('#search_input')->count());
        $this->assertEquals(1, $crawler->filter('form[action="/search"]')->count());
    }

    public function testFormulaireCreation()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertEquals(1, $crawler->filter('.connexion_form_mess')->count());
    }
}
