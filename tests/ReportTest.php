<?php

namespace App\Tests;

use App\Entity\Comment;
use App\Entity\Message;
use App\Entity\Report;
use App\Entity\Retweet;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class ReportTest extends TestCase
{
    public function testId()
    {
        $report = new Report();
        $this->assertNull($report->getId());
    }

    public function testMessageId()
    {
        $report = new Report();
        $message = new Message();
        $message->setText("pouet");
        $report->setMessageId($message);
        $this->assertEquals($message ,$report->getMessageid());
        $report->setMessageId(new Message());
        $this->assertNotEquals($message ,$report->getMessageid());
    }

    public function testCommentId()
    {
        $report = new Report();
        $message = new Comment();
        $message->setText("pouet");
        $report->setCommentId($message);
        $this->assertEquals($message ,$report->getCommentid());
        $report->setCommentId(new Comment());
        $this->assertNotEquals($message ,$report->getCommentid());
    }

    public function testRetweetId()
    {
        $report = new Report();
        $message = new Retweet();
        $message->setText("pouet");
        $report->setRetweetId($message);
        $this->assertEquals($message ,$report->getRetweetid());
        $report->setRetweetId(new Retweet());
        $this->assertNotEquals($message ,$report->getRetweetid());
    }

    public function testMessage()
    {
        $report = new Report();
        $message = "new Message()";
        $report->setMessage($message);
        $this->assertEquals($message ,$report->getMessage());
        $report->setMessage("");
        $this->assertNotEquals($message ,$report->getMessage());
    }
}
