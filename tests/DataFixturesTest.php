<?php

namespace App\Tests;

use App\DataFixtures\AppFixtures;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DataFixturesTest extends WebTestCase
{
    public function testSomething()
    {
        $client = static::createClient();
        $container = static::$container;
        $userRepository = $container->get(UserRepository::class);

        $preCount = count($userRepository->findAll());
        $dataFixtures = new AppFixtures();
        $dataFixtures->load($container->get(EntityManagerInterface::class));
        $postCount = count($userRepository->findAll());

        $this->assertEquals($preCount+10, $postCount);
    }
}
