<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminTest extends WebTestCase
{
    public function testAdminNotConnected()
    {
        $client = static::createClient();
        $client->request('GET', '/admin');
        $this->assertResponseRedirects('/login', 302,'doit rediriger vers la page de connexion');
    }

    public function testNotAdminConnected()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $userNotAdmin = $userRepository->find(2);
        $client->loginUser($userNotAdmin);
        $client->request('GET', '/admin');
        $this->assertResponseStatusCodeSame(403, 'doit avoir les droits d\'admin');
    }

    public function testAdminConnected()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);
        $userNotAdmin = $userRepository->find(1);
        $client->loginUser($userNotAdmin);
        $client->request('GET', '/admin');
        $this->assertResponseIsSuccessful();
    }
}
