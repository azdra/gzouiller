<?php

namespace App\Tests;

use App\Entity\Message;
use App\Entity\Report;
use App\Entity\Retweet;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class RetweetTest extends TestCase
{
    public function testId()
    {
        $retweet = new Retweet();
        $this->assertNull($retweet->getId());
    }

    public function testText()
    {
        $retweet = new Retweet();
        $retweet->setText("toto");
        $this->assertEquals("toto", $retweet->getText());
    }

    public function testDate()
    {
        $retweet = new Retweet();
        $date = new \DateTime();
        $retweet->setDate($date);
        $this->assertEquals($date, $retweet->getDate());
    }

    public function testMessage()
    {
        $retweet = new Retweet();
        $message = new Message();
        $message->setText("toto");
        $retweet->setMessage($message);
        $this->assertEquals($message, $retweet->getMessage());
    }

    public function testReport()
    {
        $retweet = new Retweet();
        $report = new Report();
        $report->setMessage("toto");
        $retweet->addReport($report);
        $this->assertEquals(new ArrayCollection([$report]), $retweet->getReports());
        $retweet->removeReport($report);
        $this->assertEquals(new ArrayCollection(), $retweet->getReports());
    }
}
