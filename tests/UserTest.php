<?php

namespace App\Tests;

use App\Entity\Comment;
use App\Entity\CommentLike;
use App\Entity\Follow;
use App\Entity\Message;
use App\Entity\MessageLike;
use App\Entity\Report;
use App\Entity\Retweet;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{

    public function testMessagesUser()
    {
        $user = new User();
        $preCount = count($user->getMessages());
        $entity = new Message();
        $user->addMessage($entity);
        $this->assertCount($preCount+1, $user->getMessages());
        $user->removeMessage($entity);
        $this->assertCount($preCount, $user->getMessages());
    }

    public function testRetweetsUser()
    {
        $user = new User();
        $preCount = count($user->getRetweets());
        $entity = new Retweet();
        $user->addRetweet($entity);
        $this->assertCount($preCount+1, $user->getRetweets());
        $user->removeRetweet($entity);
        $this->assertCount($preCount, $user->getRetweets());
    }

    public function testCommentsUser()
    {
        $user = new User();
        $preCount = count($user->getComments());
        $entity = new Comment();
        $user->addComment($entity);
        $this->assertCount($preCount+1, $user->getComments());
        $user->removeComment($entity);
        $this->assertCount($preCount, $user->getComments());
    }

    public function testMessageLikesUser()
    {
        $user = new User();
        $preCount = count($user->getMessageLikes());
        $entity = new MessageLike();
        $user->addMessageLike($entity);
        $this->assertCount($preCount+1, $user->getMessageLikes());
        $user->removeMessageLike($entity);
        $this->assertCount($preCount, $user->getMessageLikes());
    }

    public function testCommentLikesUser()
    {
        $user = new User();
        $preCount = count($user->getCommentLikes());
        $entity = new CommentLike();
        $user->addCommentLike($entity);
        $this->assertCount($preCount+1, $user->getCommentLikes());
        $user->removeCommentLike($entity);
        $this->assertCount($preCount, $user->getCommentLikes());
    }

    public function testFollowersUser()
    {
        $user = new User();
        $preCount = count($user->getFollowers());
        $entity = new Follow();
        $user->addFollower($entity);
        $this->assertCount($preCount+1, $user->getFollowers());
        $user->removeFollower($entity);
        $this->assertCount($preCount, $user->getFollowers());
    }

    public function testFollowingsUser()
    {
        $user = new User();
        $preCount = count($user->getFollowings());
        $entity = new Follow();
        $user->addFollowing($entity);
        $this->assertCount($preCount+1, $user->getFollowings());
        $user->removeFollowing($entity);
        $this->assertCount($preCount, $user->getFollowings());
    }

    public function testReportsUser()
    {
        $user = new User();
        $preCount = count($user->getReports());
        $entity = new Report();
        $user->addReport($entity);
        $this->assertCount($preCount+1, $user->getReports());
        $user->removeReport($entity);
        $this->assertCount($preCount, $user->getReports());
    }
}
