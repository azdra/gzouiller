<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\User;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;


class ConnexionTest extends WebTestCase
{
    public function testConnexionReussie()
    {
        $client = static::createClient();
        $client->request("GET", "/login");

        $this->assertResponseIsSuccessful();
        $client->submitForm("submit", [
            "email" => "yellow@mail.com",
            "password" => "password"
        ]);
        $this->assertResponseRedirects("/", 302, "doit rediriger vers la page d'accueil");
    }

    public function testEmailInconnuConnexion()
    {
        $client = static::createClient();
        $client->request("GET", "/login");

        $this->assertResponseIsSuccessful();
        $client->submitForm("submit", [
            "email" => "jexiste@pas.lol",
            "password" => "password"
        ]);
        $this->assertResponseRedirects("/login", 302, "doit rediriger vers la page de login");
    }
}
