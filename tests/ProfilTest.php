<?php

namespace App\Tests;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProfilTest extends WebTestCase
{
    public function testRenduAutreProfil()
    {
        $client = static::createClient();
        $client->request('GET', '/profil/@yellow');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('#user-pseudo', '@Yellow');
        $this->assertSelectorTextContains('#user-since', 'Depuis le :');
        $this->assertSelectorTextContains('#bio', 'Ce qui est mystique ce n’est pas comment est le monde, mais le fait qu’il est.');
        $this->assertSelectorTextContains('[data-target="#collapseFollowing"]', 'Mes abonnements');
        $this->assertSelectorTextContains('[data-target="#collapseFollowers"]', 'Mes abonnés');
    }

    public function testRedirectMonProfil()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->find(1);
        $client->loginUser($user);
        $client->request('GET', '/profil/');
        $this->assertResponseRedirects("/profil/@me", 302);
    }

    public function testMonProfil()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->find(1);
        $client->loginUser($user);
        $client->request('GET', '/profil/@me');
        $this->assertResponseIsSuccessful();
    }

    public function testFetchUserInconnu()
    {
        $client = static::createClient();
        $client->xmlHttpRequest('GET', '/api/users/9999');
        $this->assertResponseStatusCodeSame(404);
    }

    public function testFetchUserConnu()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->find(1);

        // messages récents
        $client->xmlHttpRequest('GET', '/api/users/messages/1');
        $this->assertResponseIsSuccessful();

        // reste du profil
        $client->xmlHttpRequest('GET', '/api/users/1.json');
        $this->assertResponseIsSuccessful();
        $data = json_decode($client->getResponse()->getContent(), 1);
        $this->assertEquals($user->getId(), $data["id"]);
        $this->assertCount(count($user->getMessages()), $data["messages"]);
        $this->assertCount(count($user->getFollowers()), $data["followers"]);
        $this->assertCount(count($user->getFollowings()), $data["followings"]);
        $this->assertCount(count($user->getComments()), $data["comments"]);
    }
}
