<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NewRetweetTest extends WebTestCase
{
    public function testSomething()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->find(1);
        $client->loginUser($user);
        $client->request("GET", "/retweet");

        $this->assertResponseIsSuccessful();
    }
}
