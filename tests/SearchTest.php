<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SearchTest extends WebTestCase
{
    public function testRechercheVide()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/search');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('#main-title', 'Résultats de la recherche ""');
        $this->assertSelectorTextContains('#users-title', "@utilisateurs");
        $this->assertSelectorTextContains('#tags-title', "#hashtags");
    }

    public function testRecherche()
    {
        $client = static::createClient();
        $client->request('GET', '/search?query=yellow');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('#main-title', 'Résultats de la recherche "yellow"');
        // requête AJAX : utilisateurs
        $client->xmlHttpRequest('GET', '/api/users/like/yellow');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString(
            'Yellow',
            $client->getResponse()->getContent()
        );
        // requête AJAX : hashtags
        $client->xmlHttpRequest('GET', '/api/hashtags/like/i');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertStringContainsString(
            'i',
            $client->getResponse()->getContent()
        );
    }
}
