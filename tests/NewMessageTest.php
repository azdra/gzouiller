<?php

namespace App\Tests;

use App\Entity\Message;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class NewMessageTest extends WebTestCase
{
    public function testRenduNewMessageForm()
    {
        $client = static::createClient();
        // connexion
        $user = static::$container->get(UserRepository::class)->findOneByEmail('yellow@mail.com');
        $client->loginUser($user);

        $crawler = $client->request('GET', '/message');

        $this->assertResponseIsSuccessful();
        $this->assertCount(1, $crawler->filter('textarea'));
        $this->assertCount(1, $crawler->filter('input[type="file"]'));
        $this->assertCount(1, $crawler->filter('input[type="number"]'));
        $this->assertCount(1, $crawler->filter('input[type="date"]'));
        $this->assertCount(1, $crawler->filter('*[type="submit"]'));
    }

    public function testAutoRemplissageAuthorNewMessageForm()
    {
        $client = static::createClient();
        // connexion
        $user = static::$container->get(UserRepository::class)->findOneByEmail('yellow@mail.com');
        $client->loginUser($user);

        $crawler = $client->request('GET', '/message');
        $submitButton = $crawler->selectButton('message-submit');
        $form = $submitButton->form();
        $authorValue = $form->getValues()[$form->getName() . '[author]'];
        $this->assertEquals($user->getId(), $authorValue);
    }

    public function testAutoRemplissageDateNewMessageForm()
    {
        $client = static::createClient();
        // connexion
        $user = static::$container->get(UserRepository::class)->findOneByEmail('yellow@mail.com');
        $client->loginUser($user);

        $crawler = $client->request('GET', '/message');
        $submitButton = $crawler->selectButton('message-submit');
        $form = $submitButton->form();
        $dateValue = $form->getValues()[$form->getName() . '[date]'];
        $dateExpected = new \DateTime();
        $this->assertEquals($dateExpected->format('Y-m-d'), $dateValue);
    }

    public function testRemplissageTooLongTextNewMessageForm()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(
            'POST',
            '/api/messages',
            [],
            [],
            [],
            json_encode([
                'text' => 'jefaisplusde140caractèresjefaisplusde140caractèresjefaisplusde140caractèresjefaisplusde140caractèresjefaisplusde140caractèresjefaisplusde140caractèresjefaisplusde140caractères',
                'author' => 1,
                'date' => "2020-05-11"
            ])
        );
        $this->assertResponseStatusCodeSame(400);
    }

    public function testRemplissageEmptyTextNewMessageForm()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(
            'POST',
            '/api/messages',
            [],
            [],
            [],
            json_encode([
                'text' => null,
                'author' => 1,
                'date' => "2020-05-11"
            ])
        );
        $this->assertResponseStatusCodeSame(400);
    }

    public function testRemplissageAuteurManquantNewMessageForm()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(
            'POST',
            '/api/messages',
            [],
            [],
            [],
            json_encode([
                'text' => 'je suis un message',
                'date' => "2020-05-11"
            ])
        );
        $this->assertResponseStatusCodeSame(500);
    }

    public function testRemplissageDateManquanteNewMessageForm()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(
            'POST',
            '/api/messages',
            [],
            [],
            [],
            json_encode([
                'text' => 'je suis un message',
                'author' => 1,
            ])
        );
        $this->assertResponseStatusCodeSame(500);
    }

    public function testRemplissageSuccessTextNewMessageForm()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(
            'POST',
            '/api/messages',
            [],
            [],
            [],
            json_encode([
                'text' => 'je suis un message',
                'author' => 1,
                'date' => "2020-05-11"
            ])
        );

        $this->assertResponseIsSuccessful();

        /*$message = static::$container->get(MessageRepository::class)->findOneBy(['text' => 'je suis un message']);
        $this->assertNotNull($message);
        $this->assertInstanceOf(Message::class, $message);
        $this->assertEquals($user->getId(), $message->getAuthor()->getId());
        $dateExpected = "2020-05-11";
        $this->assertEquals($dateExpected->format('Y-m-d'), $message->getDate()->format('Y-m-d'));
        $this->assertEquals('je suis un message', $message->getText());
        $this->assertEquals(null, $message->getImageName());
        $this->assertEquals(null, $message->getImageFile());
        $this->assertEquals(null, $message->getImageType());
        $this->assertEmpty($message->getLikes());
        $this->assertEmpty($message->getComments());
        $this->assertEmpty($message->getRetweets());*/
    }

    public function testRemplissageSuccessImageNewMessageForm()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(
            'POST',
            '/api/attached/1',
            [],
            ['file' => new UploadedFile('assets/images/sacrifice.png', 'bird.png')]);

        $this->assertResponseIsSuccessful();

        // vérification
        $client->xmlHttpRequest('GET', '/api/messages/1.json');
        $this->assertResponseIsSuccessful();
        $data = json_decode($client->getResponse()->getContent(), 1);
        $message = static::$container->get(MessageRepository::class)->find(1);
        $this->assertEquals($data["imageType"], $message->getImageType());
    }

    public function testRemplissageMauvaisParametreImageNewMessageForm()
    {
        $client = static::createClient();
        $client->xmlHttpRequest(
            'POST',
            '/api/attached/1',
            [],
            ['xxx' => new UploadedFile('assets/images/bird.png', 'bird.png')]);

        $this->assertResponseStatusCodeSame(400);
    }
}
