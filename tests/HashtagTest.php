<?php

namespace App\Tests;

use App\Entity\Hashtag;
use App\Entity\Message;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class HashtagTest extends TestCase
{
    public function testTextHashtag()
    {
        $hashtag = new Hashtag();
        $hashtag->getId();
        $text = "je suis un texte";
        $hashtag->setText($text);
        $this->assertEquals($text, $hashtag->getText());
    }
    public function testMessagesHashtag()
    {
        $hashtag = new Hashtag();
        $message = new Message();
        $hashtag->addMessage($message);
        $this->assertEquals(new ArrayCollection([$message]), $hashtag->getMessages());
        $hashtag->removeMessage($message);
        $this->assertEquals(new ArrayCollection(), $hashtag->getMessages());
    }
}
