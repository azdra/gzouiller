import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../scss/app.scss';
import $ from 'jquery';

$.when($.ready).then(() => {
  const loader = $('#loader');
  $(document).ajaxStart(() => {
    loader.fadeIn();
  });
  $(document).ajaxStop(() => {
    loader.fadeOut();
  });
});

