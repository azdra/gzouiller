/**
 * Lit un string pour vérifier s'il vaut "true"
 * @param {string} s
 * @return {boolean}
 */
export const parseBoolean = (s) => /^true$/.test(s);

export const recognizedFileTypes = [
  'image/png',
  'image/jpeg',
  'image/bmp',
  'image/svg+xml',
  'image/gif',
  'image/webp',
  'video/x-msvideo',
  'video/mpeg',
  'video/ogg',
  'video/webm',
  'video/3gpp',
  'video/3gpp2',
  'audio/aac',
  'audio/midi',
  'audio/mpeg',
  'audio/ogg',
  'audio/x-wav',
  'audio/webm',
  'audio/3gpp',
  'audio/3gpp2',
];
