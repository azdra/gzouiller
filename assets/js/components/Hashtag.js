import $ from 'jquery';

/**
 * Représente un hashtag
 */
export default class Hashtag {
  /**
   * Crée un objet représentant un hashtag
   * @param {string} text
   */
  constructor(text) {
    this.text = text;
  }

  /**
   * Retourne l'élément HTML/jQuery représentant ce hashtag
   * @return {jQuery|HTMLElement}
   */
  createView() {
    return $(this.toString());
  }

  /**
   * Retourne l'élément HTML correspondant, sous forme de string
   * @return {string}
   */
  toString() {
    return `<a class="tweet-tag" href="/hashtag?query=${this.text}">
#${this.text}</a>`;
  }
}
