import $ from 'jquery';

export default class Contact {
  constructor(data) {
    this.data = data;
  }

  createView() {
    const viewContact = $(`
        <div class="card-body rounded" id="message-${this.data.id}">
            <div class="container mx-5 bg-content-50">
                <div class="row p-2">
                    <div class="card-body-header d-flex w-100"></div>
                        <a href="/profil/@${this.data.author.pseudo}" title="Voir le profil de ${this.data.author.pseudo}" class="avatar_link">
                            <div>
                                <span class="placeholder-avatar placeholder-avatar-mini rounded-circle d-flex justify-content-center align-items-center"><span>${this.data.author.pseudo.slice(0, 1)}</span></span>
                            </div>
                            <div class="col-8 px-2 ml-1">
                                <a class="d-block tweet-author" href="/profil/@${this.data.author.pseudo}" id="message-${this.data.id}-author">${this.data.author.pseudo}</a>
                                <span class="d-block text-tiny ml-1 text-white"><span id="message-${this.data.id}-abonnes">${this.data.author.followers ? this.data.author.followers.length : 0}</span> abonné(e)s</span>
                            </div>
                        </a>
                        <div class="card-body-body w-100 mt-2 py-2">
                        <div class="tweet-container mx-2 position-relative">
                            <div class="tweet-content">
                                <div class="tweet-text mb-3 text-white w-auto">
                                    ${this.data.message}
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                </div>
            </div>
        </div>
    `);
    return viewContact;
  }
}
