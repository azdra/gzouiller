/* eslint-disable max-len */
import $ from 'jquery';
// images
import threeDots from '../../images/three_dots.svg';
import heart from '../../images/heart.svg';
import heartActive from '../../images/heart-active.svg';
import bird from '../../images/bird.svg';
import comment from '../../images/comment.svg';
import link from '../../images/link.svg';
// entités
import Hashtag from './Hashtag';
import Mention from './Mention';
import Comment from './Comment';
// bibliothèques
import Swal from 'sweetalert2';
import dayjs from 'dayjs';

// URL
const url = location.href;

/**
 * Représente un Message
 */
export default class Message {
  /**
   * Crée un Message représentant un message stocké dans la BDD,
   * avec cet id en particulier.
   * @param {JSON} data - les données du message
   * @param {boolean} isConnected - si l'utilisateur est connecté ou non
   * @param {boolean} isAdmin - si l'utilisateur est un admin ou non
   * @param {number} userId - si connecté, id de l'utilisateur
   * @param {boolean} voted - si l'utilisateur a liké ou non
   */
  constructor(data, isConnected, isAdmin = false, userId = 0, voted = false) {
    this.isConnected = isConnected;
    this.isAdmin = isAdmin;
    // pour enlever les avertissements
    this.data = {
      id: 0,
      text: '',
      imageName: null,
      imageType: null,
      comments: null,
      likes: null,
      retweets: null,
      author: {
        id: 0,
        pseudo: '',
        followers: null,
        followings: null,
      },
      hashtags: null,
    };
    this.data = data;
    this.userId = userId;
    this.voted = voted;
  }

  /**
   * Renvoie l'élément jQuery représentant le message
   * TODO : afficher l'avatar de l'auteur
   * @return {jquery}
   */
  createView() {
    // --- MENTIONS ---
    this.data.text = Mention.parseMentions(this.data.text);
    // --- MESSAGE ENTIER ---
    const view = $(`
<div class="card-body rounded" id="message-${this.data.id}"> 
  <div class="container">
      <div class="row">
          <div class="card-body-header d-flex w-100">
            <a href="/profil/@${this.data.author.pseudo}" title="Voir le profil de ${this.data.author.pseudo}" class="avatar_link">
              <div>
                  <span class="placeholder-avatar placeholder-avatar-mini rounded-circle d-flex justify-content-center align-items-center"><span>${this.data.author.pseudo.slice(0, 1)}</span></span>
              </div>
              <div class="col-8 px-2 ml-1">
                  <a class="d-block tweet-author" href="/profil/@${this.data.author.pseudo}" id="message-${this.data.id}-author">${this.data.author.pseudo}</a>
                  <span class="d-block text-tiny ml-1 text-white"><span id="message-${this.data.id}-abonnes">${this.data.author.followers ? this.data.author.followers.length : 0}</span> abonné(e)s</span>
              </div>
            </a>
              <div class="col-4 d-flex align-items-center justify-content-center">
                  <button ${!this.isConnected ? 'disabled' : ''} class="btn mb-auto dropdown-toggle" title="Plus" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="img d-inline-block">
                          <img class="img" src="${threeDots}" alt="Plus">
                      </span>
                  </button>
                  <div class="dropdown-menu tweet-dropdown-right" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item text-white" id="message-${this.data.id}-see-profile" href="/profil/@${this.data.author.pseudo}">Voir le profil</a>
                      <a class="dropdown-item text-white" id="message-${this.data.id}-follow">Suivre l'auteur</a>
                      <a class="dropdown-item text-white" id="message-${this.data.id}-signal" href="/report/message/${this.data.id}">Signaler le message</a>
                  </div>
              </div>
          </div>
          <div class="card-body-body w-100 mt-2 py-2">
              <div class="tweet-container mx-2 position-relative">
                  <div class="tweet-content">
                      <div class="tweet-text mb-3 text-white">
                          <span id="message-${this.data.id}-text">${this.data.text}</span>
                          <div class="text-muted mt-3">${new Date(this.data.date).toLocaleString('fr')}</div>
                      </div>
                      <div id="message-${this.data.id}-attached"></div>
                  </div>
                  <div class="tweet-footer container-fluid pt-2">
                      <div class="row justify-content-between">
                          <div id="tweet-likes" class="like">
                              <button ${!this.isConnected ? 'disabled' : ''} class="btn px-2 text-tiny" title="Likes" id="message-${this.data.id}-likes">
                                  <span class="img d-inline-block">
                                      <img class="img" data-id="1" data-liked="0" id="message-${this.data.id}-heart" src="${this.voted ? heartActive : heart}" alt="like">
                                  </span>
                                  <span class="nbn-likes d-inline-block text-white"><span id="message-${this.data.id}-likes-count">${this.data.likes ? this.data.likes.length : '0'}</span> likes</span>
                              </button>
                          </div>
                          <div id="tweet-gazouillis" class="gazouillis">
                              <button ${!this.isConnected ? 'disabled' : ''} class="btn px-2 text-tiny" title="Gazouillis" id="message-${this.data.id}-retweets">
                                  <span class="img d-inline-block">
                                      <img class="img" src="${bird}" alt="re-gazouiller">
                                  </span>
                                  <span class="nbn-likes d-inline-block text-white"><span id="message-${this.data.id}-retweets-count">${this.data.retweets ? this.data.retweets.length : '0'}</span> re-gazouillis</span>
                              </button>
                          </div>
                          <div id="tweet-comments" class="comment">
                              <button class="btn px-2 text-tiny" type="button" data-toggle="collapse" data-target="#comment-collapse-${this.data.id}" aria-expanded="false" aria-controls="comment-collapse-${this.data.id}" title="Commentaires" id="message-${this.data.id}-comments">
                                  <span class="img d-inline-block">
                                      <img class="img" src="${comment}" alt="commenter">
                                  </span>
                                  <span class="nbn-likes d-inline-block text-white"><span id="message-${this.data.id}-comments-count">${this.data.comments ? this.data.comments.length : '0'}</span> commentaires</span>
                              </button>
                          </div>
                          <div id="tweet-sharer" class="share">
                              <button class="btn img d-inline-block px-2 text-tiny" title="Partager" id="message-${this.data.id}-share">
                                  <span class="img d-inline-block">
                                      <img class="img" src="${link}" alt="partager">
                                  </span>
                              </button>
                          </div>
                      </div>
                  </div>
                  <div class="collapse" id="comment-collapse-${this.data.id}">
                  <div class="tweet-comments-content mt-2">
                    <div class="d-flex justify-content-center">
                        <div id="loader-comment" class="loader-comment-${this.data.id}"></div>
                    </div>
                    <div id="comment-${this.data.id}-container" class="tweet-comments-container border-top p-0 mt-2"></div> 
                    <div class="tweet-comments-content mt-2 text-white d-flex flex-row">
                        <input ${!this.isConnected ? 'disabled' : ''} type="text" placeholder="Crée un commentaire" id="input-${this.data.id}-comment" class="form-control w-100 rounded input-comment mr-2">
                        <button ${!this.isConnected ? 'disabled' : ''} class="btn bg-primary" id="btn-${this.data.id}-comment" type="submit" style="border-radius: 3px">Envoyer</button>
                    </div>
                    ${this.isConnected ? '' : `
                    <p style="color: white; font-size: 12px" class="required">Pour pouvoir publier un commentaire, veuillez vous
                        <a href="/login">connecter</a>
                        ou vous
                        <a href="/register">inscrire</a>
                    </p>`}
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>`);
    // --- PARTAGER ---
    view.find(`#message-${this.data.id}-share`).click(() => {
      const copyToClipboard = (id) => {
        const el = document.createElement('textarea');
        el.value = `${url}message/${id}`;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Lien copié avec succès dans le presse papier',
          showConfirmButton: false,
          timer: 1300,
        });
      };

      copyToClipboard($(this)[0].data.id);
    });
    // --- HASHTAGS ---
    const messageText = view.find(`#message-${this.data.id}-text`);
    this.data.hashtags?.forEach((link) => {
      $.get(link).then((tag) => {
        const hashtag = new Hashtag(tag.text);
        messageText.append(hashtag.createView());
      });
    });
    if (this.isAdmin) {
      // --- SUPPRIMER ---
      const deleteButton = $(`<a class="dropdown-item text-danger" id="message-${this.data.id}-delete" href="#">Supprimer (Admin)</a>`);
      deleteButton.click(async () => {
        await $.ajax(`/api/messages/${this.data.id}`, {
          method: 'DELETE',
        });
        location.href = '/';
      });
      deleteButton.insertAfter(view.find(`#message-${this.data.id}-signal`));
    }

    if (this.isConnected) {
      // --- LIKE ---
      const likeCount = view.find(`#message-${this.data.id}-likes-count`);
      view.find(`#message-${this.data.id}-likes`).click( async () => {
        // LET DON'T CONST
        const imgHeart = $(`#message-${this.data.id}-heart`);
        if (this.voted === false) {
          $.post({
            url: '/api/message_likes',
            data: JSON.stringify({
              message: '/api/messages/' + this.data.id,
              author:
                '/api/users/' + this.userId,
            }),
            success: (e) => {
              let count = parseInt(likeCount.text());
              count++;
              likeCount.text(count);
              imgHeart.attr('src', heartActive);
              this.voted = e.id;
            },
            contentType: 'application/json',
          });
        } else {
          await $.ajax('/api/message_likes/'+this.voted, {
            method: 'DELETE',
          }).done(() => {
            let count = parseInt(likeCount.text());
            count--;
            likeCount.text(count);
            imgHeart.attr('src', heart);
            this.voted = false;
          });
        }
      });
      // --- RETWEETS ---
      view.find(`#message-${this.data.id}-retweets`).click(() => {
        location.href = '/retweet/?message=' + this.data.id;
      });
      // --- FOLLOW ---
      const abonnesCount = view.find(`#message-${this.data.id}-abonnes`);
      view.find(`#message-${this.data.id}-follow`).click(() => {
        $.post({
          url: '/api/follows',
          data: JSON.stringify({
            followTarget: '/api/users/' + this.data.author.id,
            followSource: '/api/users/' + this.userId,
          }),
          success: () => {
            let count = parseInt(abonnesCount.text());
            count++;
            abonnesCount.text(count);
          },
          contentType: 'application/json',
        });
      });
      // --- SIGNAL ---
      view.find(`#message-${this.data.id}-signal`).click(() => {
        // TODO signaler
        console.warn(`TODO signal ${this.data.id}`);
      });

      // --------------------------
      //          COMMENT
      // --------------------------
      // --- COMMENT ADD ---
      view.find(`#input-${this.data.id}-comment`).on('keydown', (e) => {
        const target = e.target;
        const event = e.originalEvent;
        if (event.charCode === 13 || event.key === 'Enter') {
          if (target.value) this.addComment(target.value);
        }
      });

      // --- BTN SEND COMMENT ---
      view.find(`#btn-${this.data.id}-comment`).on('click', async () => {
        const input = $(`#input-${this.data.id}-comment`)[0];
        if (input.value) this.addComment(input.value);
      });
    }

    // --- AFFICHAGE COMMENT ---
    // Check si #comment-collapse-${this.data.id} à la class show
    view.find(`#message-${this.data.id}-comments`).on('click', async () => {
      if ( $(`#comment-collapse-${this.data.id}`).hasClass('show') ) return;
      const commentLoader = $(`.loader-comment-${this.data.id}`);
      await commentLoader.removeClass('d-none');

      const cmt = await $.get('/api/messages/' + this.data.id);
      const arrayComments = cmt.comments;

      // Récupère la data de son profil
      const selfData = await $.get('/api/users/'+this.userId);

      // Supprime tout de comment-${this.data.id}-container
      const commentContainer = $(`#comment-${this.data.id}-container`);
      commentContainer.text('');

      let liked = false;
      for (const commentData of arrayComments) {
        const likes = commentData.likes;

        for (const messageLike of likes) {
          // Boucle sur l'array de nos messages liké
          for (const dataUserLike of selfData.commentLikes) {
            // Regarde si
            // /api/message_likes/12 === /api/message_likes/12 slipt au 3ème slash
            // 12 === 12
            // Si 12 === 12 alors voted = 12
            if (messageLike.split('/')[3] === dataUserLike.split('/')[3]) {
              liked = messageLike.split('/')[3];
            }
          }
        }
        const comment = await new Comment(
            this.data,
            commentData,
            this.isConnected,
            this.isAdmin,
            this.userId,
            parseInt(liked),
        );

        await commentContainer.append(comment.createView());
        liked = false;
      }

      // Ajout la class "d-none" au loader
      commentContainer.slideDown('slow');
      await commentLoader.addClass('d-none');
    });
    // comment-${this.data.id}-signal
    // pièce jointe
    if (this.data.imageName) {
      this.loadAttached(view).then();
    }
    return view;
  }

  /**
   * --- PIÈCE JOINTE ---
   * Va chercher la pièce et l'affiche
   * @param {jQuery} view - vue du message
   */
  async loadAttached(view) {
    const url = '/api/attached/'+this.data.id;
    const attached = view.find(`#message-${this.data.id}-attached`);
    attached.addClass('row justify-content-center tweet-img');
    switch (this.data.imageType) {
      case 'image/png':
      case 'image/jpeg':
      case 'image/bmp':
      case 'image/svg+xml':
      case 'image/gif':
      case 'image/webp':
        attached.append(`<img class="p-2 img-fluid" src="${url}" alt="${this.data.imageName}">`);
        break;
      case 'video/x-msvideo':
      case 'video/mpeg':
      case 'video/ogg':
      case 'video/webm':
      case 'video/3gpp':
      case 'video/3gpp2':
        attached.append(`<video src="${url}" controls>TODO</video>`);
        break;
      case 'audio/aac':
      case 'audio/midi':
      case 'audio/mpeg':
      case 'audio/ogg':
      case 'audio/x-wav':
      case 'audio/webm':
      case 'audio/3gpp':
      case 'audio/3gpp2':
        attached.append(`<audio src="${url}" controls>TODO</audio>`);
        break;
      default:
        attached.append(`<div class="text-danger">Format (${this.data.imageType}) non reconnu</div>`);
        console.warn('format non reconnu ' + this.data.imageType);
    }
  }

  /**
   * Ajoute un commentaire au message
   * @param {string} text - le texte du commentaire
   */
  addComment(text) {
    $.post({
      url: '/api/comments',
      data: JSON.stringify({
        text: text,
        author: '/api/users/' + this.userId,
        message: '/api/messages/' + this.data.id,
        date: dayjs().format('YYYY-MM-DD'),
      }),
      success: async () => {
        const {value} = await Swal.fire({
          title: '👏',
          text: 'Ah, ça, c\'est du commentaire !',
          icon: 'success',
          showCancelButton: true,
          confirmButtonText: 'Aller voir le message',
          cancelButtonText: 'Rester sur cette page',
        });
        if (value) {
          location.href = '/message/' + this.data.id;
        }
      },
      contentType: 'application/json',
    });
  }
}
