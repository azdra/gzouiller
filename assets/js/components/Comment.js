/* eslint-disable max-len */
import $ from 'jquery';
// images
import threeDots from '../../images/three_dots.svg';
import heart from '../../images/heart.svg';
import heartActive from '../../images/heart-active.svg';
// style
import '../../scss/components/comment.scss';
import Mention from './Mention';

/**
 * Représente un Commentaire
 */
export default class Comment {
  /**
   * Crée un Comment représentant un commentaire stocké dans la BDD,
   * avec cet id en particulier.
   * @param {JSON} dataMessage - les données du commentaire
   * @param {JSON} dataComment - les données du commentaire
   * @param {boolean} isConnected - si l'utilisateur est connecté ou non
   * @param {boolean} isAdmin - si l'utilisateur est un admin ou non
   * @param {number} userId - si connecté, id de l'utilisateur
   * @param {number} liked - si commentaire liké, id du comment_like
   */
  constructor(dataMessage, dataComment, isConnected, isAdmin = false, userId = 0, liked = 0) {
    // pour enlever les avertissements
    this.dataMessage = {
      id: 0,
      text: '',
      imageName: null,
      imageType: null,
      comments: null,
      likes: null,
      retweets: null,
      author: {
        id: 0,
        pseudo: '',
        followers: null,
        followings: null,
      },
    };
    this.dataMessage = dataMessage;
    this.dataComment = {
      author: {
        id: 0,
        pseudo: '',
      },
      text: '',
      likes: null,
    };
    this.dataComment = dataComment;
    this.userId = userId;
    this.liked = liked;
    this.isConnected = isConnected;
    this.isAdmin = isAdmin;
  }

  /**
   * Renvoie l'élément jQuery représentant le commentaire
   * TODO : afficher l'avatar de l'auteur
   * @return {jquery}
   */
  createView() {
    // --- MENTIONS ---
    this.dataComment.text = Mention.parseMentions(this.dataComment.text);
    const viewComment = $(`
            <div class="bg-content-50 p-1 m-2 shadow">
            <div class="card-body-header d-flex w-100 mt-1">
                <a href="/profil/@${this.dataComment.author.pseudo}" title="Voir le profil de ${this.dataComment.author.pseudo}">
                  <div>
                    <span class="placeholder-avatar placeholder-avatar-mini rounded-circle d-flex justify-content-center align-items-center"><span>${this.dataComment.author.pseudo.slice(0, 1)}</span></span>
                  </div>
                  <div class="col-8 px-2 ml-1">
                    <a class="d-block tweet-author" href="/profil/@${this.dataComment.author.pseudo}" id="message-${this.dataComment.author.followers.length}-author">${this.dataComment.author.pseudo}</a>
                    <span class="d-block text-tiny ml-1 text-white"><span id="message-${this.dataComment.author.followers.length}-abonnes">${this.dataComment.author.followers ? this.dataComment.author.followers.length : 0}</span> abonné(e)s</span>
                  </div>
                </a>
                <div class="col-2 px-2 d-flex align-items-center">
                  <button class="btn ml-auto mb-auto dropdown-toggle" title="Plus" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="img d-inline-block">
                          <img class="img" src="${threeDots}" alt="Plus">
                      </span>
                  </button>
                  <div class="dropdown-menu tweet-dropdown-right" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item text-white" data-message="${this.dataMessage.id}" id="comment-${this.dataComment.id}-see-profile" href="/profil/@${this.dataComment.author.pseudo}">Voir le profil</a>
                      <a class="dropdown-item text-white" data-message="${this.dataMessage.id}" id="comment-${this.dataComment.id}-follow">Suivre l'auteur</a>
                      <a class="dropdown-item text-white" data-message="${this.dataMessage.id}" id="comment-${this.dataComment.id}-signal" href="/report/comment/${this.dataComment.id}">Signaler le commentaire</a>
                  </div>
                </div>
                </div>
                <div class="card-body-body w-100 mt-2 py-2">
                  <div class="tweet-container mx-2">
                    <div class="tweet-content">
                      <div class="tweet-text text-white">
                        ${this.dataComment.text}
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tweet-footer container-fluid pt-2">
                    <div class="row justify-content-between">
                        <div id="tweet-likes" class="like text-tiny">
                            <button ${this.isConnected ? '' : 'disabled'} class="btn px-2" title="Likes" data-message="${this.dataMessage.id}" id="comment-${this.dataComment.id}-likes">
                                <span class="img d-inline-block">
                                    <img class="img" id="comment-${this.dataComment.id}-heart" src="${this.liked ? heartActive : heart}" alt="like">
                                </span>
                                <span class="nbn-likes d-inline-block text-white">
                                    <span data-message="${this.dataMessage.id}" id="comment-${this.dataComment.id}-likes-count">${this.dataComment.likes ? this.dataComment.likes.length : '0'}</span>
                                    likes
                                </span>
                            </button>
                        </div>
                        <button ${this.isConnected ? '' : 'disabled'} class="btn text-primary d-flex align-items-center" id="comment-${this.dataComment.id}-answer"><span class="curved-arrow"></span>Répondre</button>
                    </div>
                </div>
            </div>
          </div>`);

    if (this.isAdmin) {
      // --- SUPPRIMER ---
      const deleteButton = $(`<a class="dropdown-item text-danger" id="comment-${this.dataComment.id}-delete" href="#">Supprimer (Admin)</a>`);
      deleteButton.click(async () => {
        await $.ajax(`/api/comments/${this.dataComment.id}`, {
          method: 'DELETE',
        });
        location.href = '/';
      });
      deleteButton.insertAfter(viewComment.find(`#comment-${this.dataComment.id}-signal`));
    }

    if (this.isConnected) {
      // --- LIKE ---
      const likeCount = viewComment.find(`#comment-${this.dataComment.id}-likes-count`);
      viewComment.find(`#comment-${this.dataComment.id}-likes`).click( async () => {
        const imgHeart = $(`#comment-${this.dataComment.id}-heart`);
        if ( this.liked === false || isNaN(this.liked) ) {
          $.post({
            url: '/api/comment_likes',
            data: JSON.stringify({
              comment: '/api/comments/' + this.dataComment.id,
              author:
                '/api/users/' + this.userId,
            }),
            success: (e) => {
              let count = parseInt(likeCount.text());
              count++;
              likeCount.text(count);
              imgHeart.attr('src', heartActive);
              this.liked = e.id;
            },
            contentType: 'application/json',
          });
        } else {
          await $.ajax('/api/comment_likes/'+this.liked, {
            method: 'DELETE',
          }).done(() => {
            let count = parseInt(likeCount.text());
            count--;
            likeCount.text(count);
            imgHeart.attr('src', heart);
            this.liked = false;
          });
        }
      });
      // --- RÉPONDRE ---
      const commentInput = $(`#input-${this.dataMessage.id}-comment`);
      viewComment.find(`#comment-${this.dataComment.id}-answer`).click(() => {
        commentInput.val(`@${this.dataComment.author.pseudo} ` + commentInput.val());
      });
    }
    return viewComment;
  }
}
