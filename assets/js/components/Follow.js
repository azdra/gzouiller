import $ from 'jquery';

/**
 * Représente un Follower ou un Following
 */
export default class Follow {
  /**
   * Crée un lien représentant un follower/following stocké dans la BDD,
   * @param {JSON} data - les données du follow/follower
   */
  constructor(data) {
    this.data = data;
  }
  /**
   * Renvoie l'élément jQuery représentant le follower ou le following
   * @return {jquery}
   */
  createView() {
    const view = $(`
<div class="card-body p-2">
  <div id="user-container" style="display: none" data-id="${this.data.id}"></div>
  <div class="container">
      <div class="row">
          <div class="card-body-header d-flex w-100">
            <a href="/profil/@${this.data.pseudo}" alt="Voir le profil de ${this.data.pseudo}" class="avatar_link">
              <div class="">
                  <span class="placeholder-avatar placeholder-avatar-mini rounded-circle d-flex justify-content-center align-items-center">
                    <span>${this.data.pseudo.slice(0, 1)}</span>
                   </span>
              </div>
              <div class="col-8 px-2 ml-1">
                  <a class="d-block tweet-author" href="/profil/@${this.data.pseudo}">${this.data.pseudo}</a>
                  <span class="d-block text-tiny ml-1 text-white"><span id="message-${this.data.id}-abonnes">${this.data.followers ? this.data.followers.length : 0}</span> abonné(e)s</span>
              </div>
            </a>
      </div>
  </div>
</div>
    `);
    return view;
  }
};
