import $ from 'jquery';

/**
 * Représente une mention
 */
export default class Mention {
  /**
   * Crée un objet représentant une mention
   * @param {string} pseudo
   */
  constructor(pseudo) {
    this.pseudo = pseudo;
  }

  /**
   * Retourne l'élément HTML/jQuery représentant cette mention
   * @return {jQuery|HTMLElement}
   */
  createView() {
    return $(this.toString());
  }

  /**
   * Retourne l'élément HTML correspondant, sous forme de string
   * @return {string}
   */
  toString() {
    return `<a class="tweet-tag" href="/profil/@${this.pseudo}">
@${this.pseudo}</a>`;
  }

  /**
   * Cherche les mentions (@pseudo) et les remplace
   * par l'élément HTML correspondant à la mention
   * @param {string} text
   * @return {string}
   */
  static parseMentions(text) {
    const mentions = new Set(text.match(/@\w+/g));
    mentions.forEach((m) => {
      const mention = new Mention(m.slice(1));
      text = text.replace(new RegExp(m, 'g'), mention.toString());
    });
    return text;
  }
}
