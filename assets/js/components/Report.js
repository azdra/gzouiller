import $ from 'jquery';

/**
 * Représente un signalement
 */
export default class Report {
  /**
   * Construit un objet Report
   * /!\ il faut getTargetData
   * @param {JSON[]} data - les données de(s) signalement(s)
   * @param {string} target - l'IRI (/api/...) de la cible
   */
  constructor(data, target) {
    this.target = target;
    this.data = data;
    this.targetType = target.split('/')[2]; // TYPE DE LA CIBLE ?
    this.targetId = target.split('/')[3]; // ID DE LA CIBLE ?
  }

  /**
   * Crée et renvoie la vue de ce Report
   * @return {jQuery}
   */
  createView() {
    return $(this.createHTML());
  }

  /**
   * Renvoie l'HTML du Report
   * @return {string}
   */
  createHTML() {
    let view = `
    <div class="card border border-white m-3 p-3">
      <div class="card-header mb-3">
      <h4 class="card-title">
        <a href="${this.getLink()}" class="card-link">${this.getTitle()}</a>
      </h4>
      <h5 class="card-subtitle">${this.targetData[this.getIdentifierKey()]}</h5>
      </div>
      
      <div class="accordion" id="accordion-${this.targetId}">`;

    // AJOUT DES RAPPORTS INDIVIDUELS
    this.data.forEach((report) => {
      const date = new Date(report['date']).toLocaleString('fr');
      view += `<div class="card report-container rounded">
        <div class="card-header p-2" id="report-${report['id']}-title">
          <h6 class="mb-0">
            <button class="btn btn-link btn-block report-title"
            type="button"
            data-toggle="collapse" data-target="#report-${report['id']}"
            aria-expanded="true" aria-controls="report-${report['id']}">
              Signalement ${report['id']} (${date})
            </button>
          </h6>
        </div>
        <div id="report-${report['id']}"
        class="collapse"
        aria-labelledby="report-${report['id']}-title"
        data-parent="#accordion-${this.targetId}">
          <div class="card-body report-body p-2">
          ${report['message']}
          </div>
        </div>
      </div>`;
    });

    // FERMETURE DES BALISES
    view += '</div></div>';
    return view;
  }

  /**
   * Renvoie la cible du signalement, quel que soit son targetType
   * @param {JSON} json
   * @return {string}
   */
  static getTarget(json) {
    if (json.messageid) return json.messageid;
    if (json.commentid) return json.commentid;
    if (json.userid) return json.userid;
  }

  /**
   * Retourne la clé servant à "identifier" de
   * manière lisible la cible
   * @return {string}
   */
  getIdentifierKey() {
    switch (this.targetType) {
      case 'users':
        return 'pseudo';
      case 'comments':
      case 'messages':
        return 'text';
      default:
        throw new Error('targetType de cible inconnu : ' + this.targetType);
    }
  };

  /**
   * Retourne la clé servant à "identifier" de
   * manière lisible la cible
   * @return {string}
   */
  getTitle() {
    let title;
    switch (this.targetType) {
      case 'users':
        title = 'Utilisateur ';
        break;
      case 'comments':
        title = 'Commentaire ';
        break;
      case 'messages':
        title = 'Message ';
        break;
      default:
        throw new Error('targetType de cible inconnu : ' + this.targetType);
    }
    return title + this.targetId;
  };

  /**
   * Renvoie un lien vers la cible
   * @return {string}
   */
  getLink() {
    switch (this.targetType) {
      case 'users':
        return '/profil/@' + this.targetData['pseudo'];
      case 'comments':
        return '/message/' + this.targetData['message']['id'];
      case 'messages':
        return '/message/' + this.targetData['id'];
      default:
        throw new Error('targetType de cible inconnu : ' + this.targetType);
    }
  }

  /**
   * Fetch les données de la cible
   * @return {Promise<void>}
   */
  async getTargetData() {
    this.targetData = await $.get(this.target+'.json');
  }
}
