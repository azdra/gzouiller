import $ from 'jquery';
import Follow from './Follow';

// RECUPERE L'ID DE L'UTILISATEUR DUQUEL ON VISITE LE PROFIL
const userId = $('#user-container').data('id');
console.log(userId);

fetch(`/api/users/${userId}`)
    .then((response) => {
      return response.json();
    })
    .then((transformation) => {
      console.log(transformation);
      // RECUPERE LA LISTE DES FOLLOWERS
      const followers = transformation.followers;
      const followings = transformation.followings;

      // BOUCLE SUR LES FOLLOWERS
      for (let i = 0; i < followers.length; ++i) {
        fetch(`${followers[i]}`)
            .then((response) => {
              return response.json();
            })
            .then((transformation) => {
              // RECUPERE UN FOLLOWER
              const follower = transformation.followSource;
              fetch(`${follower}`)
                  .then((response) => {
                    return response.json();
                  })
                  .then((transformation) => {
                    // GENERE LES LIENS VERS LES FOLLOWERS
                    const follow = new Follow(transformation);
                    $('#follower_cards').append(follow.createView());
                  });
            });
      }

      // BOUCLE SUR LES FOLLOWINGS
      for (let i = 0; i < followings.length; ++i) {
        fetch(`${followings[i]}`)
            .then((response) => {
              return response.json();
            })
            .then((transformation) => {
              // RECUPERE UN FOLLOWING
              const following = transformation.followTarget;
              fetch(`${following}`)
                  .then((response) => {
                    return response.json();
                  })
                  .then((transformation) => {
                    // GENERE LES LIENS VERS LES FOLLOWING
                    const follow = new Follow(transformation);
                    $('#following_cards').append(follow.createView());
                  });
            });
      }
    });
