import axios from 'axios';
import Swal from 'sweetalert2';

document.addEventListener('DOMContentLoaded', () => {
  console.log('Load form contact modo');

  const minCharacter = 8;
  const maxCharacter = 255;

  const form = document.getElementById('contact_moderator');
  const textarea = document.getElementById('contact_moderator_message');
  const submitBtn = document.getElementById('submit_contact_moderator');
  const minChar = document.getElementById('contact_moderator_min_chart');
  const maxChar = document.getElementById('contact_moderator_max_chart');
  const good = document.getElementById('contact_moderator_min_good');
  submitBtn.setAttribute('disabled', true);

  textarea.value = '';
  maxChar.innerText = maxCharacter;
  minChar.innerText = minCharacter;

  textarea.addEventListener('keyup', (e) => {
    const areaLength = textarea.value.length;
    maxChar.innerText = (maxCharacter - areaLength)+'';

    if (areaLength > maxCharacter) {
      if (maxChar.classList.contains('text-primary')) {
        maxChar.classList.remove('text-primary');
        maxChar.classList.add('text-danger');
      }
    } else {
      if (maxChar.classList.contains('text-danger')) {
        maxChar.classList.remove('text-danger');
        maxChar.classList.add('text-primary');
      }
    }

    if (areaLength >= minCharacter) {
      if (submitBtn.getAttribute('disabled')) {
        good.innerText = 'GOOD';
        good.classList.add('text-success');
        good.classList.remove('text-danger');
      }
      submitBtn.removeAttribute('disabled');
    }
    if (areaLength <= minCharacter-1) {
      good.innerText = 'BAD';
      good.classList.add('text-danger');
      good.classList.remove('text-success');
      submitBtn.setAttribute('disabled', true);
    }
  });

  form.addEventListener('submit', (e) => {
    e.preventDefault();

    // enlève les éventuels caractères vides
    const value = textarea.value.trim();
    console.log(form.getAttribute('data-user'));

    if (textarea.value.length > maxCharacter) {
      return Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Je crois que tu à un peu abusé au niveau des caractères!',
      });
    }

    axios({
      method: 'POST',
      url: '/api/contact_moderators',
      data: JSON.stringify({
        'author': form.getAttribute('data-user'),
        'message': value,
      }),
      headers: {'Content-Type': 'application/json; charset=utf-8'},
    }).then( (res) => {
      console.log(res);

      let timerInterval;
      Swal.fire({
        title: 'Message envoyé avec succès.',
        html: 'Redirection dans <b></b>s.',
        timer: 2000,
        timerProgressBar: true,
        willOpen: () => {
          Swal.showLoading();
          timerInterval = setInterval(() => {
            const content = Swal.getContent();
            if (content) {
              const b = content.querySelector('b');
              if (b) {
                b.textContent = ((Swal.getTimerLeft()/1000) % 60).toFixed(2);
              }
            }
          }, 100);
        },
        onClose: () => {
          clearInterval(timerInterval);
          location.href = '../';
        },
      });
    }).catch( (e) => {
      console.error(e);
    });
  });
});
