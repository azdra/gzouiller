import dayjs from 'dayjs';
import axios from 'axios';
import Swal from 'sweetalert2';

document.addEventListener('DOMContentLoaded', () => {// FORMULAIRE MESSAGE
  const form = document.getElementById('new_message');
  const input = document.getElementById('new_message_text');
  input.value = input.value.trim(); // enlève les éventuels caractères vides
  const file = document.getElementById('new_message_imageFile_file');
  const submit = document.getElementById('message-submit');

  // RÉCUPÈRE ID D'UTILISATEUR
  const userId = document.getElementById('message-container').dataset.id;

  // COMPTEUR DE CARACTÈRES
  const MsgCharRest = 140;

  // AFFICHE LE NOMBRE DE CARACTÈRES RESTANTS A CHAQUE CHANGEMENTS DANS L'INPUT
  const charCount = document.getElementById('charCount');
  input.addEventListener('input', function(event) {
    const MsgCharCount = input.value.length;
    charCount.innerText = (MsgCharRest - MsgCharCount)+'';
  });

  // VALIDATION
  const validateForm = (form, event) => {
    input.value = input.value.trim();
    event.preventDefault();
    event.stopPropagation();
    form.classList.add('was-validated');
    submit.setAttribute('disabled', '');
    if (file.files.length > 0 && file.files[0].size > 2000000) {
      file.classList.add('is-invalid');
    } else {
      file.classList.remove('is-invalid');
      if (form.checkValidity()) {
        submit.removeAttribute('disabled');
      }
    }
  };

  input.addEventListener('blur', (event) => validateForm(form, event));
  file.addEventListener('change', (event) => validateForm(form, event));

  form.addEventListener('submit', async function(event) {
    // LIRE LES DONNÉES
    const data = {
      author: userId,
      text: input.value.trim(),
      date: dayjs().format(),
    };
    validateForm(form, event);
    let result;
    try {
      // POSTER LE MESSAGE & LIRE LE RÉSULTAT
      result = await axios.post('/api/messages', data);
      console.log(result);
      // poster la pièce jointe
      if (file.files.length > 0) {
        console.log(file.files[0]);
        const formData = new FormData();
        formData.append('file', file.files[0]);
        await axios.post('/api/attached/' + result.data.id, formData);
      }
      // ALERTE DE SUCCÈS
      const {value} = await Swal.fire({
        title: '👏',
        text: 'Wallah comme tu l\'as posté ce message !',
        icon: 'success',
        showCancelButton: true,
        confirmButtonText: 'Aller voir le message',
        cancelButtonText: 'Rester sur cette page',
      });
      // REDIRECTION
      if (value) {
        location.href = '/message/' + result.data.id;
      }
    } catch (e) {
      if (result) {
        // SI ENREGISTRÉ LE MESSAGE MAIS ERREUR PIÈCE JOINTE
        // -> EFFACER LE MESSAGE
        await axios.delete('/api/messages/' + result.data.id);
      }
      await Swal.fire('Oups', 'Enregistrement du message échoué', 'error');
    }
  });
});
