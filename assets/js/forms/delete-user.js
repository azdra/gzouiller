import $ from 'jquery';
import Swal from 'sweetalert2';

const btnDelete = $('#btn-delete');
const userID = $('#user-container').data('id');

btnDelete.on('click', function() {
  $.ajax({
    url: `/api/users/${userID}`,
    type: 'DELETE',
    success: function() {
      console.log('ok');
    },
  });

  Swal.fire({
    position: 'center',
    icon: 'success',
    title: 'Utilisateur supprimé avec succès',
    showConfirmButton: false,
    timer: 1500,
  });

  location.href = '../../';
});
