import $ from 'jquery';

const fileLabel = document.querySelector('.custom-file label');
const imgUploaded = document.getElementById('registration_form_avatarFile_file');
const refreshAvatar = document.getElementById('refreshAvatar');

const readURL = (input) => {
  if (input.files && input.files[0]) {
    $('#refreshAvatar').css('display', 'block');
    $('#avatar_block').css('display', 'block');
    $('#preview_profile_avatar').css('display', 'block');
    const reader = new FileReader();

    reader.onload = (e) => {
      $('#preview_profile_avatar').css({
        'backgroundImage': `url(${e.target.result})`,
        'height': '150px',
        'width': '150px',
        'clipPath': 'circle(40%)',
      });
      fileLabel.innerHTML = input.files[0].name;
    };

    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
};

const refreshInputFile = () => {
  $('#refreshAvatar').css('display', 'none');
  $('#avatar_block').css('display', 'none');
  $('#preview_profile_avatar').css({
    'display': 'none',
    'backgroundImage': '',
  });
  imgUploaded.value = '';
  fileLabel.innerHTML = 'importer un fichier';
};

refreshAvatar.addEventListener('click', () => refreshInputFile());
imgUploaded.addEventListener('change', () => readURL(imgUploaded));
