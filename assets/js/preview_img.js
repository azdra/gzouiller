import $ from 'jquery';

const imgPreview = document.getElementById('img_preview');
const fileLabel = document.getElementById('new_message_imageFile_file_label');
const imgUploaded = document.getElementById('new_message_imageFile_file');
const refreshButton = document.getElementById('refresh_input_file');

const displayRefreshBtn = () => {
  if ($('#img_preview').hasClass('empty')) {
    $('#refresh_input_file').css('display', 'none');
  } else {
    $('#refresh_input_file').css('display', 'block');
  }
};

const readURL = (input) => {
  if (input.files && input.files[0]) {
    $('#img_preview').toggleClass('empty');
    displayRefreshBtn();
    const reader = new FileReader();

    reader.onload = (e) => {
      imgPreview.src = e.target.result;
      fileLabel.innerHTML = input.files[0].name;
    };

    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
};

const refreshInputFile = () => {
  $('#img_preview').toggleClass('empty');
  displayRefreshBtn();
  imgUploaded.value = '';
  imgPreview.src = '';
  fileLabel.innerHTML = 'Ajouter un fichier (Audio, Video, Image)';
};

refreshButton.addEventListener('click', () => refreshInputFile());
imgUploaded.addEventListener('change', () => readURL(imgUploaded));
