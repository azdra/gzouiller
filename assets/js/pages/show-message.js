import Message from '../components/Message';
import $ from 'jquery';
import {parseBoolean} from '../utils';

document.addEventListener('DOMContentLoaded', async () => {
  const messageContainer = $('#message-container');
  const isConnected = parseBoolean(messageContainer.data('connected'));
  const isAdmin = parseBoolean(messageContainer.data('admin'));
  const userId = messageContainer.data('id');
  const url = location.href.split('/');
  const id = parseInt(url[url.length-1]);
  try {
    const res = await $.get('/api/messages/' + id);
    console.log(res);
    const message = new Message(res, isConnected, isAdmin, userId);
    messageContainer.append(message.createView());
  } catch (e) {
    messageContainer.append(`<h2 class="text-white text-center">
Message introuvable</h2>`);
  }
});
