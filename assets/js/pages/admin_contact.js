import $ from 'jquery';
import Contact from '../components/Contact.js';

$.when($.ready).then(async () => {
  const contact_admin_container = $('#contact_admin_container');

  const contactList = await $.get('/api/contact_moderators.json');
  if (contactList.length >= 1) {
    for (const data of contactList) {
      const message = await new Contact(data);
      await contact_admin_container.append(message.createView());
    };
  };
});
