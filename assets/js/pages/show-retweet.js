// import Message from './Message';
import $ from 'jquery';
// import {parseBoolean} from './utils';

document.addEventListener('DOMContentLoaded', async () => {
  // const messageContainer = $('#message-container');
  const retweetContainer = $('#retweet-container');
  // const isConnected = parseBoolean(messageContainer.data('connected'));
  // const isAdmin = parseBoolean(messageContainer.data('admin'));
  // const userId = messageContainer.data('id');
  const url = location.href.split('/');
  const retweetId = parseInt(url[url.length-1]);
  try {
    const retweet = await $.get('/api/retweets/' + retweetId);
    console.log(retweet);
    if (retweet.text) {
      retweetContainer.prepend(retweet.text);
    }
    // const message = new Message(res, isConnected, isAdmin, userId);
    // messageContainer.append(message.createView());
  } catch (e) {
    retweetContainer.append(`<h2 class="text-white text-center">
Re-gazouilli introuvable</h2>`);
  }
});
