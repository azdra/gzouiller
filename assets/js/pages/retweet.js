import $ from 'jquery';
import dayjs from 'dayjs';
import Message from '../components/Message';

$.when($.ready).then(async ()=> {
  const messageContainer = $('#message-container');
  const isConnected = messageContainer.data('connected');
  const isAdmin = messageContainer.data('admin');
  const userId = messageContainer.data('id');
  const messageId = location.search.slice(location.search.search('message=')+8);
  console.log(messageId);
  $.get('/api/messages/'+messageId).then((data) => {
    const message = new Message(data, isConnected, isAdmin, userId);
    messageContainer.append(message.createView());
    console.log(data);
  });
  // formulaire
  const form = document.getElementsByTagName('form')[0];
  const input = document.getElementById('new_retweet_text');
  form.addEventListener('submit', (e) => {
    e.preventDefault();
    $.post({
      url: '/api/retweets',
      data: JSON.stringify({
        author: '/api/users/'+userId,
        text: input.value,
        message: '/api/messages/'+messageId,
        date: dayjs().format('YYYY-MM-DD'),
      }),
      contentType: 'application/json',
    });
  });
});
