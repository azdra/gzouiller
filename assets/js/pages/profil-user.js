import $ from 'jquery';
import Message from '../components/Message';
import Follow from '../components/Follow';

$.when($.ready).then(async () => {
  const messageContainer = $('#message-container');
  const isConnected = messageContainer.data('connected');
  const isAdmin = messageContainer.data('admin');
  const userId = messageContainer.data('id');
  const profileId = messageContainer.data('profile');

  // ---- MESSAGES RÉCENTS ----
  const messageList = await $.get('/api/users/messages/'+parseInt(profileId));
  messageList.forEach((data) => {
    const message = new Message(data, isConnected, isAdmin, userId);
    messageContainer.append(message.createView());
  });

  // ---- FOLLOWS ----
  fetch(`/api/users/${profileId}`)
      .then((response) => {
        return response.json();
      })
      .then((transformation) => {
      // RECUPERE LA LISTE DES FOLLOWERS
        const followers = transformation.followers;
        const followings = transformation.followings;

        // BOUCLE SUR LES FOLLOWERS
        for (let i = 0; i < followers.length; ++i) {
          fetch(`${followers[i]}`)
              .then((response) => {
                return response.json();
              })
              .then((transformation) => {
                // RECUPERE UN FOLLOWER
                const follower = transformation.followSource;
                fetch(`${follower}`)
                    .then((response) => {
                      return response.json();
                    })
                    .then((transformation) => {
                      // GENERE LES LIENS VERS LES FOLLOWERS
                      const follow = new Follow(transformation);
                      $('#follower_cards').append(follow.createView());
                    });
              });
        }

        // BOUCLE SUR LES FOLLOWINGS
        for (let i = 0; i < followings.length; ++i) {
          fetch(`${followings[i]}`)
              .then((response) => {
                return response.json();
              })
              .then((transformation) => {
                // RECUPERE UN FOLLOWING
                const following = transformation.followTarget;
                fetch(`${following}`)
                    .then((response) => {
                      return response.json();
                    })
                    .then((transformation) => {
                      // GENERE LES LIENS VERS LES FOLLOWING
                      const follow = new Follow(transformation);
                      $('#following_cards').append(follow.createView());
                    });
              });
        }
      });
});
