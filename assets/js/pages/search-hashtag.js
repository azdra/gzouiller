import $ from 'jquery';
import Message from '../components/Message';

$.when($.ready).then(async () => {
  const hashtagName = location.search.slice(location.search.search('=')+1);
  document.title += ' #'+hashtagName;
  $('#tags-title').text('#'+hashtagName);
  const container = $('#tags');
  try {
    const hashtag = await $.get('/api/hashtags/' + hashtagName);
    console.log(hashtag);
    // affichage des messages
    const isConnected = container.data('connected');
    const isAdmin = container.data('admin');
    const userId = container.data('id');
    for (const messageLink of hashtag.messages) {
      const data = await $.get(messageLink);
      console.log(data);
      const message = new Message(data, isConnected, isAdmin, userId);
      container.append(message.createView());
    }
  } catch (e) {
    container.append(`<div class="text-danger text-center font-weight-bold">
Aucun message ne contient #${hashtagName}
</div>`);
  }
});
