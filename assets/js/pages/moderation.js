import '../../scss/components/side-panel.scss';
import axios from 'axios';
import Report from '../components/Report';

document.addEventListener('DOMContentLoaded', () => {
  const toggle = document.getElementById('signal-panel-toggle');
  const panel = document.getElementById('signal-panel');
  const container = document.getElementById('signal-container');
  const orderedReportList = {};
  let isShown = false;
  let isAnimated = true;

  // MONTRER / CACHER LE PANNEAU
  const togglePanel = () => {
    if (isShown) {
      panel.style.left = '-' + panel.clientWidth + 'px';
      toggle.style.left = '-1.5rem';
    } else {
      toggle.style.left = 'calc(' + panel.clientWidth + 'px - 1.5rem)';
      panel.style.left = '0';
    }
    isShown = !isShown;
    // 1ère fois
    if (isAnimated) {
      isAnimated = false;
      toggle.style.left = '-1.5rem';
      toggle.style.animationName = 'none';
      toggle.style.left = 'calc(' + panel.clientWidth + 'px - 1.5rem)';
    }
  };

  // ACTIONS LORS DU CLIC SUR LE BOUTON
  toggle.addEventListener('click', () => {
    console.log('click');
    // MONTRER / CACHER LE PANNEAU
    togglePanel();

    // RÉCUPÉRATION DES DONNÉES
    if (Object.keys(orderedReportList).length === 0) {
      axios.get('/api/reports/ordered').then((resultFetch) => {
        const reportList = resultFetch.data;
        // TRI DES DONNÉES
        reportList.forEach((report) => {
          const target = Report.getTarget(report);
          // AJOUT À LA LISTE
          if (!orderedReportList[target]) {
            orderedReportList[target] = [report];
          } else {
            orderedReportList[target].push(report);
          }
        });
        // AFFICHER LES SIGNALEMENTS
        for (const target in orderedReportList) {
          if (orderedReportList.hasOwnProperty(target)) {
            const report = new Report(orderedReportList[target], target);
            report.getTargetData().then(() => {
              container.innerHTML += report.createHTML();
            });
          }
        }
      });
    }
  });
});
