import 'chart.js';
import 'dayjs';
import $ from 'jquery';
import dayjs from 'dayjs';

// HTML CANVAS
const ctx = document.getElementById('chartTest').getContext('2d');

// CHART DATAS

// TOTAL NEW MESSAGES
const totalNewMessages = [0, 0, 0, 0, 0, 0, 0];

// TOTAL NEW USERS
const totalNewUsers = [0, 0, 0, 0, 0, 0, 0];

// CURRENT WEEK
const currentWeek = [
  dayjs().subtract(6, 'day').format('DD/MM/YYYY'),
  dayjs().subtract(5, 'day').format('DD/MM/YYYY'),
  dayjs().subtract(4, 'day').format('DD/MM/YYYY'),
  dayjs().subtract(3, 'day').format('DD/MM/YYYY'),
  dayjs().subtract(2, 'day').format('DD/MM/YYYY'),
  dayjs().subtract(1, 'day').format('DD/MM/YYYY'),
  dayjs().format('DD/MM/YYYY'),
];

// CHART
const chartTest = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: currentWeek,
    datasets: [
      {
        label: 'Nouveaux messages',
        backgroundColor: 'rgba(255, 99, 132, 0.2)',
        borderColor: 'rgba(255, 99, 132, 1)',
        borderWidth: 1,
      },
      {
        label: 'Nouveaux utilisateurs',
        backgroundColor: 'rgba(64, 224, 208, 0.2)',
        borderColor: 'rgba(64, 224, 208, 1)',
        borderWidth: 1,
      },
    ],
  },
  options: {
    maintainAspectRatio: false,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
        },
      }],
    },
  },
});

Promise.all([
  // GET TOTAL USERS
  fetch('api/users')
      .then((response) => {
        return response.json();
      })
      .then((transformation) => {
        chartTest.data.datasets[1].data = totalNewUsers;
        const arrayUsers = transformation['hydra:member'];
        $('#admin_total_users').html(arrayUsers.length);

        // USERS WHO REGISTERS TODAY
        let todayInscriptions = 0;
        for (let i = 0; i < arrayUsers.length; ++i) {
          const userCreation = dayjs(arrayUsers[i].created_at)
              .format('DD/MM/YYYY');

          // CHECK IF THEY REGISTER THEMSELVES TODAY
          if (userCreation === currentWeek[6]) {
            ++todayInscriptions;
            ++totalNewUsers[6];
          } else if (userCreation === currentWeek[5]) {
            ++totalNewUsers[5];
          } else if (userCreation === currentWeek[4]) {
            ++totalNewUsers[4];
          } else if (userCreation === currentWeek[3]) {
            ++totalNewUsers[3];
          } else if (userCreation === currentWeek[2]) {
            ++totalNewUsers[2];
          } else if (userCreation === currentWeek[1]) {
            ++totalNewUsers[1];
          } else if (userCreation === currentWeek[0]) {
            ++totalNewUsers[0];
          }
        }

        // ADD DATA ON CHART
        chartTest.data.datasets[1].data = totalNewUsers;
        chartTest.update();

        // ADD ON DAILY VIEW
        $('#admin_total_users_today').html(todayInscriptions);
      }),

  // GET GLOBALE MESSAGES STATS
  fetch('api/messages')
      .then((response) => {
        return response.json();
      })
      .then((transformation) => {
        // GET AN ARRAY OF ALL MESSAGES
        const arrayMsg = transformation['hydra:member'];

        // GET ARRAY LENGTH
        $('#admin_total_messages').html(arrayMsg.length);

        // STATS INIT
        let comments = 0;
        let likes = 0;
        let retweets = 0;
        let todayMessages = 0;

        // EXTRACT STATS ON EACH MESSAGE
        for (let i = 0; i < arrayMsg.length; ++i) {
          // GET TOTAL COMMENTS/LIKES/RETWEETS
          comments += arrayMsg[i].comments.length;
          likes += arrayMsg[i].likes.length;
          retweets += arrayMsg[i].retweets.length;

          // MSG DATE
          const msgDate = new Date(arrayMsg[i].date).toLocaleDateString();

          // CHECK IF MESSAGE WAS POSTED TODAY
          if (msgDate === currentWeek[6]) {
            ++todayMessages;
          }

          const msgCreation = dayjs(arrayMsg[i].date)
              .format('DD/MM/YYYY');

          // CHECK IF THEY REGISTER THEMSELVES TODAY
          if (msgCreation === currentWeek[6]) {
            ++totalNewMessages[6];
          } else if (msgCreation === currentWeek[5]) {
            ++totalNewMessages[5];
          } else if (msgCreation === currentWeek[4]) {
            ++totalNewMessages[4];
          } else if (msgCreation === currentWeek[3]) {
            ++totalNewMessages[3];
          } else if (msgCreation === currentWeek[2]) {
            ++totalNewMessages[2];
          } else if (msgCreation === currentWeek[1]) {
            ++totalNewMessages[1];
          } else if (msgCreation === currentWeek[0]) {
            ++totalNewMessages[0];
          }
        }

        // GET AVERAGES AND PRINT THEM
        // likes
        const likesPerMsg = Math.ceil(likes / arrayMsg.length);
        $('#admin_total_likes').html(`${likes} (${likesPerMsg} / message)`);
        // comments
        const commentsPerMsg = Math.ceil(comments / arrayMsg.length);
        $('#admin_total_comments').html(
            `${comments} (${commentsPerMsg} / message)`);
        // retweets
        const retweetsPerMsg = Math.ceil(retweets / arrayMsg.length);
        $('#admin_total_retweets').html(
            `${retweets} (${retweetsPerMsg} / message)`);

        // ADD DATA ON CHART
        chartTest.data.datasets[0].data = totalNewMessages;
        chartTest.update();

        // ADD DATA ON DAILY VIEW
        $('#admin_daily_messages').html(todayMessages);
      }),
]);
