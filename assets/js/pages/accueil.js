import $ from 'jquery';
import Message from '../components/Message';

$.when($.ready).then(async () => {
  const messageContainer = $('#message-container');
  const isConnected = messageContainer.data('connected');
  const isAdmin = messageContainer.data('admin');
  const userId = messageContainer.data('id');

  const messageList = await $.get('/api/messages.json');
  // messageList = messageList.slice(0, 5);

  let selfData = null;
  if (userId) {
    // Récupère la data de son profil
    selfData = await $.get('/api/users/'+userId);
  }

  // Définit le variable voted sur false
  // Laisser sur false
  let voted = false;
  for (const data of messageList) {
    if (userId) {
      // récupère array de tous les likes
      const likes = data.likes;
      // Boucle sur l'array des likes du message
      for (const messageLike of likes) {
        // Boucle sur l'array de nos messages liké
        for (const dataUserLike of selfData.messageLikes) {
          // Regarde si
          // /api/message_likes/12 === /api/message_likes/12 slipt au 3ème slash
          // 12 === 12
          // Si 12 === 12 alors voted = 12
          if (messageLike.split('/')[3] === dataUserLike.split('/')[3]) {
            voted = messageLike.split('/')[3];
          }
        }
      }
    }

    // Crée un nouveau message sur la constance message
    const message = await new Message(data, isConnected, isAdmin, userId, voted);
    // Append la constance message dans le messageContainer
    await messageContainer.append(message.createView());
    // Re-définit voted sur false
    voted = false;
  }

  // messageList.forEach( (data) => {
  //   console.log(data);
  //
  //   const likes = data.likes;
  //   const likesData = await $.get(likes);
  //
  //   const message = new Message(data, isConnected, isAdmin, userId);
  //   messageContainer.append(message.createView());
  // });
});
