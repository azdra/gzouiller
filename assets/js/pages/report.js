import axios from 'axios';
import Swal from 'sweetalert2';

document.addEventListener('DOMContentLoaded', () => {
  // LECTURE DES PARAMÈTRES
  const urlParts = location.href.split('/');
  const type = urlParts[urlParts.length-2]; // avant-dernier
  const targetId = urlParts[urlParts.length-1]; // dernier
  let typeText;
  const data = {};
  switch (type) {
    case 'user':
      typeText = ' un utilisateur';
      data['userid'] = `/api/users/${targetId}`;
      break;
    case 'comment':
      data['commentid'] = `/api/comments/${targetId}`;
      typeText = ' un commentaire';
      break;
    case 'message':
      data['messageid'] = `/api/messages/${targetId}`;
      typeText = ' un message';
      break;
    default:
      throw new Error(`type de cible de signalement inconnu ! (${type})`);
  }
  // COMPLÉTION DES TITRES
  // TODO : afficher l'utilisateur/message/commentaire visé
  document.title += typeText;
  document.getElementsByTagName('h1')[0].innerText += typeText + '...';

  // ÉLÉMENTS HTML À MANIPULER
  const form = document.getElementsByTagName('form')[0];
  const input = document.getElementById('signal_message');
  input.value = input.value.trim(); // enlève les éventuels caractères vides
  const submit = document.getElementById('signal_submit');

  // AFFICHE LE NOMBRE DE CARACTÈRES RESTANTS A CHAQUE CHANGEMENTS DANS L'INPUT
  const MsgCharRest = 255;
  const charCount = document.getElementById('charCount');
  input.addEventListener('input', function(event) {
    const MsgCharCount = input.value.length;
    charCount.innerText = (MsgCharRest - MsgCharCount)+'';
  });

  // VALIDATION
  const validateForm = (form, event) => {
    input.value = input.value.trim();
    event.preventDefault();
    event.stopPropagation();
    form.classList.add('was-validated');
    if (form.checkValidity()) {
      submit.removeAttribute('disabled');
    } else {
      submit.setAttribute('disabled', '');
    }
  };
  input.addEventListener('blur', (event) => validateForm(form, event));

  form.addEventListener('submit', (event) => {
    event.preventDefault();
    data['message'] = input.value.trim();
    data['date'] = new Date().toISOString();
    axios.post('/api/reports', data)
        .then(() => {
          Swal.fire({
            title: 'Signalement effectué avec succès',
            text: 'Nos équipes s\'en chargeront le plus tôt possible.',
            icon: 'success',
          });
        })
        .catch(() => {
          Swal.fire({
            title: 'Erreur lors de l\'enregistrement du signalement',
            text: 'Veuillez vérifier d\'avoir rempli ' +
              'correctement le formulaire, et si oui, ' +
              'contactez notre support technique.',
            icon: 'error',
          });
        });
  });
});
