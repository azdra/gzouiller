import $ from 'jquery';
import Hashtag from '../components/Hashtag';
import Follow from '../components/Follow';

$.when($.ready).then(async () => {
  const query = location.search.slice(location.search.search('query=')+6);
  // ---------------
  //      USERS
  // ---------------
  const userWarn = () => {
    userContainer.append(`<div class="text-danger">
Il n'y a aucun @utilisateur de ce nom</div>`);
  };
  const userContainer = $('#users');
  $.get('/api/users/like/' + query)
      .then((users) => {
        console.log(users);
        if (users.length === 0) {
          userWarn();
        } else {
          users.forEach((data) => {
            const user = new Follow(data);
            userContainer.append(user.createView());
          });
        }
        $('#users-title')
            .append(` : ${users.length} résultat${users.length > 1 ?'s':''}`);
      })
      .catch((e) => {
        console.warn('recherche users échouée');
        $('#users-title').append(` : recherche échouée`);
        userWarn();
      });
  // ---------------
  //    HASHTAGS
  // ---------------
  const tagsWarn = () => {
    tagContainer.append(`<div class="text-danger">
Il n'y a aucun #tag de ce nom</div>`);
  };
  const tagContainer = $('#tags');
  $.get('/api/hashtags/like/' + query)
      .then((tags) => {
        console.log(tags);
        if (tags.length === 0) {
          tagsWarn();
        } else {
          tags.forEach((data) => {
            const tag = new Hashtag(data.text);
            tagContainer.append(tag.createView());
          });
        }
        $('#tags-title')
            .append(` : ${tags.length} résultat${tags.length > 1 ?'s':''}`);
      })
      .catch((e) => {
        console.warn('recherche tags échouée');
        $('#tags-title').append(` : recherche échouée`);
        tagsWarn();
      });
});
