import axios from 'axios';
import $ from 'jquery';

document.addEventListener('DOMContentLoaded', () => {
  const form = document.getElementsByTagName('form')[0];
  const id = form.dataset.id;
  console.log('user ' + id);
  const pseudo = document.getElementById(form.name+'_pseudo');
  const bio = document.getElementById(form.name+'_bio');
  const passwordFirst = document.getElementById(
      form.name+'_plainPassword_first');
  const passwordSecond = document.getElementById(
      form.name+'_plainPassword_second');
  const file = document.getElementById(form.name+'_avatarFile_file');
  // vérification de modification
  let hasPseudo = false;
  let hasBio = false;
  pseudo.addEventListener('change', () => {
    hasPseudo = true;
  });
  bio.addEventListener('change', () => {
    hasBio = true;
  });
  // --- envoi du formulaire ---
  form.addEventListener('submit', async (e) => {
    e.preventDefault();
    // check same password
    if (passwordFirst.value !== passwordSecond.value) {
      throw new Error('Les mots de passe doivent être identique');
    }
    try {
      const object = {};
      if (hasPseudo) {
        object['pseudo'] = pseudo.value;
      } if (hasBio) {
        object['bio'] = bio.value;
      }
      if (passwordFirst.value) {
        object['password'] = passwordFirst.value;
      }
      if (hasPseudo || hasBio || passwordFirst.value) {
        const result = await axios.patch('/api/users/' + id, object, {
          headers: {'content-type': 'application/merge-patch+json'},
        });
        console.log('Succès !');
        console.log(result);
      } else if (file.files.length > 0) {
        const formData = new FormData();
        formData.append('file', file.files[0]);
        console.log(formData);
        const result = await axios.post('/api/avatar/'+id, formData);
        console.log('Mis à jour de l\'avatar !');
        console.log(result);
      } else {
        console.warn('Aucun changement');
      }
      hasPseudo = false;
      hasBio = false;
      passwordFirst.value = '';
      passwordSecond.value = '';
    } catch (e) {
      console.error(e);
    }
  });
});

const fileLabel = document.getElementById('edit_profil_avatarFile_file');
const imgUploaded = document.getElementById('edit_profil_avatarFile_file');
const refreshAvatar = document.getElementById('refreshAvatar');

const readURL = (input) => {
  if (input.files && input.files[0]) {
    $('#refreshAvatar').css('display', 'block');
    $('#avatar_block').css('display', 'block');
    $('#preview_profile_avatar').css('display', 'block');
    const reader = new FileReader();

    reader.onload = (e) => {
      $('#preview_profile_avatar').css({
        'backgroundImage': `url(${e.target.result})`,
        'height': '150px',
        'width': '150px',
        'clipPath': 'circle(40%)',
      });
      fileLabel.innerHTML = input.files[0].name;
    };

    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
};

const refreshInputFile = () => {
  $('#refreshAvatar').css('display', 'none');
  $('#avatar_block').css('display', 'none');
  $('#preview_profile_avatar').css({
    'display': 'none',
    'backgroundImage': '',
  });
  imgUploaded.value = '';
  fileLabel.innerHTML = 'importer un fichier';
};

refreshAvatar.addEventListener('click', () => refreshInputFile());
imgUploaded.addEventListener('change', () => readURL(imgUploaded));
